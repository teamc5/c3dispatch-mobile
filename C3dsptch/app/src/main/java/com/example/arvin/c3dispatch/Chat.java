package com.example.arvin.c3dispatch;

/**
 * Created by Aaron on 19/02/2018.
 */


import com.google.firebase.firestore.IgnoreExtraProperties;
import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;

import com.google.firebase.firestore.IgnoreExtraProperties;
import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

@IgnoreExtraProperties
public class Chat extends AbstractChat {
    private String mName;
    private String mMessage;
    private String mUid;
    private Date mTimestamp;
    private String mUrl;
    private String mType;

    public Chat() {
        // Needed for Firebase
    }

    public Chat(String name, String message, String uid, String type) {
        mName = name;
        mMessage = message;
        mUid = uid;
        mType = type;

    }

    public Chat(String name, String message, String uid, String Url, String type){
        mName = name;
        mMessage = message;
        mUid = uid;
        mUrl = Url;
        mType = type;
    }

    public String getType(){ return mType; }

    public void setType (String type) {mType = type; }

    public String getUrl() { return mUrl; }

    public void setUrl(String mImgUrl) { this.mUrl = mImgUrl; }

    public String getSender() {
        return mName;
    }

    public void setSender(String name) {
        mName = name;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getUid() {
        return mUid;
    }

    public void setUid(String uid) {
        mUid = uid;
    }

    @ServerTimestamp
    public Date getTimestamp() {
        return mTimestamp;
    }

    public void setTimestamp(Date timestamp) {
        mTimestamp = timestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Chat chat = (Chat) o;

        return mTimestamp.equals(chat.mTimestamp)
                && mUid.equals(chat.mUid)
                && (mName == null ? chat.mName == null : mName.equals(chat.mName))
                && (mMessage == null ? chat.mMessage == null : mMessage.equals(chat.mMessage));
    }

    @Override
    public int hashCode() {
        int result = mName == null ? 0 : mName.hashCode();
        result = 31 * result + (mMessage == null ? 0 : mMessage.hashCode());
        result = 31 * result + mUid.hashCode();
        result = 31 * result + mTimestamp.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Chat{" +
                "mName='" + mName + '\'' +
                ", mMessage='" + mMessage + '\'' +
                ", mUid='" + mUid + '\'' +
                ", mTimestamp=" + mTimestamp +
                '}';
    }




}