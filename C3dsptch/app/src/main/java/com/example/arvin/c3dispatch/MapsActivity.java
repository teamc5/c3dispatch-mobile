package com.example.arvin.c3dispatch;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.arvin.c3dispatch.googledirection.DirectionCallback;
import com.example.arvin.c3dispatch.googledirection.GoogleDirection;
import com.example.arvin.c3dispatch.googledirection.constant.TransportMode;
import com.example.arvin.c3dispatch.googledirection.model.Direction;
import com.example.arvin.c3dispatch.googledirection.model.Line;
import com.example.arvin.c3dispatch.googledirection.model.Route;
import com.example.arvin.c3dispatch.googledirection.util.DirectionConverter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@SuppressLint("ValidFragment")
public class MapsActivity extends Fragment implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,DirectionCallback,
        LocationListener{

    private static final String TAG = "MapsActivity";
    FirebaseDatabase firebaseDatabase;
    DatabaseReference testReference, responderFireReference;
    DocumentReference documentReference;
    CollectionReference eventReference,responderReference;

    private com.google.firebase.firestore.Query query1;

    private GoogleMap mMap;

    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private static final int MY_PERMISSION_REQUEST_CODE = 7171;
    private static final int PLAY_SERVICES_RES_REQUEST = 7172;
    private static int UPDATE_INTERVAL = 5000;
    private static int FASTEST_INTERVAL = 3000;
    private static int DISTANCE = 10;
    private FusedLocationProviderClient mFusedLocationClient;
    private String collectionName;

    private GeoPoint geoPoint;
    private final String STATUS_KEY = "status";
    private final String LOCATION_KEY = "location";
    private final String USER_KEY = "username";

    private GeoPoint resLocation,eventMapLocation;
    private String resName1,eventName,eventType,eventStatus,eventLocation,locationEvent,resStatus,resName,resType;

    private String docID;

    Map<String, Object> myLoc,eventData,resData;

    private Boolean check,reqCheck=false,polCheck=true,statCheck=true,toastCheck=true;

    public Marker resMarker,eventMarker;
    public ArrayList<Marker> resArrayMarker,eventArrayMarker;
    private Button mapButton,directionButton,trafficButton;
    private String uname;
    private FirebaseAuth mAuth;


    private String serverKey = "AIzaSyD5GhEsUUAE5S620vU4p0y25GSpe3mqxPY";
    private LatLng origin = new LatLng(14.315163, 121.083269);
    private LatLng destination = new LatLng(14.321858, 121.100392);

    private LatLng destinasyon,lokasyonko;

    private LayoutInflater layoutInflater;
    private PopupWindow popupWindow;
    private FrameLayout relativeLayout;

    private ArrayList<LatLng> directionPositionList;

    private Polyline polyline;

    private AlertDialog.Builder builder;

    private List<GeoPoint> markerLoc;
    private Switch eventSwitch;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_map, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        mapButton = getView().findViewById(R.id.map_button);
        eventSwitch = getView().findViewById(R.id.switch1);

        directionButton = getView().findViewById(R.id.clear_button);
        uname = mAuth.getCurrentUser().getUid();
        check = true;
        directionPositionList = new ArrayList<>();
        relativeLayout = getView().findViewById(R.id.map_relative_layout);

        if (savedInstanceState == null) {
            Bundle extras = getActivity().getIntent().getExtras();
            if (extras == null) {
                collectionName = null;
            } else {
                collectionName = extras.getString("collectionName");
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(getContext(), android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(getContext());
        }
        //Firebase FireStore

        documentReference = FirebaseFirestore.getInstance().collection("Responders").document();
        responderReference = FirebaseFirestore.getInstance().collection("Responders");
        eventReference = FirebaseFirestore.getInstance().collection("Events");

        //Firebase Realtime Database
        responderFireReference = FirebaseDatabase.getInstance().getReference("Respondersasdasd").child("TEst");

        firebaseDatabase = FirebaseDatabase.getInstance();
        testReference = firebaseDatabase.getReference("asd").child("asdasd");
        testReference.setValue("asd");

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getContext());
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
            }, MY_PERMISSION_REQUEST_CODE);
        }
        else {
            if (checkPlayService()) {
                buildGoogleApiClient();
                createLocationRequest();
                displayLocation();
            } else {
                Log.d("PICA", "Failed to checkplayService onViewCreated");
            }
        }


        setupSystemNew();
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map1);
        mapFragment.getMapAsync(this);
        loadResponderLocation();
        loadEventLocation();
        changeMap();

    }//End of oncreatedView

    private void requestDirection(){
        GoogleDirection.withServerKey(serverKey)
                .from(lokasyonko)
                .to(destinasyon)
                .transportMode(TransportMode.DRIVING)
                .execute(this);
    }

    private void changeMap(){

        eventSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(eventSwitch.isChecked()){
                    mMap.setTrafficEnabled(true);
                }else{

                    mMap.setTrafficEnabled(false);
                }
            }
        });
        mapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //List of items to be show in  alert Dialog are stored in array of strings/char sequences  final
                final String[] items = {"RoadMap", "Hybrid","Night"};

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                //set the title for alert dialog
                builder.setTitle("Select Map Type: ");
//
//                Switch switchval = new Switch(getContext());
//                final TextView titleTextView = new TextView(getContext());
//                titleTextView.setText("asdasd");
//                titleTextView.setTextSize(15);
//                titleTextView.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
//
//
//                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)mapButton.getLayoutParams();
//                switchval.setLayoutParams(params);
//                RelativeLayout relative = new RelativeLayout(getContext());
//
//                relative.addView(titleTextView);
//                relative.addView(switchval);
//
//                builder.setCustomTitle(relative);
                //set items to alert dialog. i.e. our array , which will be shown as list view in alert dialog
                builder.setItems(items, new DialogInterface.OnClickListener() {

                    @Override public void onClick(DialogInterface dialog, int item) {
                        //setting the button text to the selected itenm from the list
                        // mapButton.setText(items[item]);
                        if (items[item].equals("RoadMap"))
                        {
                            mMap.setMapStyle(null);
                            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

                        }
                        if (items[item].equals("Hybrid"))
                        {
                            mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);

                        }
                        if (items[item].equals("Night"))
                        {
                            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                            MapStyleOptions style =MapStyleOptions.loadRawResourceStyle(getContext(),R.raw.night_map);
                            mMap.setMapStyle(style);
                        }

                    }
                });

                //Creating CANCEL button in alert dialog, to dismiss the dialog box when nothing is selected
                builder .setCancelable(false)
                        .setNegativeButton("CANCEL",new DialogInterface.OnClickListener() {

                            @Override  public void onClick(DialogInterface dialog, int id) {
                                //When clicked on CANCEL button the dalog will be dismissed
                                dialog.dismiss();
                            }
                        });

                //Creating alert dialog
                AlertDialog alert =builder.create();

                //Showingalert dialog
                alert.show();
            }
        });//End on Click Listener

    }

    private void loadResponderLocation(){
        resArrayMarker = new ArrayList<>();
        responderReference.get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot documentSnapshots) {

                for(int x=0;x<documentSnapshots.size();x++) {
                    resData = documentSnapshots.getDocuments().get(x).getData();
                    resLocation=((GeoPoint)resData.get("location"));
                    resStatus=((String)resData.get("status"));
                    resType=((String)resData.get("userType"));
                    resName=((String)resData.get("username"));

                    LatLng latLng = new LatLng(resLocation.getLatitude(),resLocation.getLongitude());

                    resMarker = mMap.addMarker(new MarkerOptions()
                            .title(resName)
                            .snippet(resStatus)
                            .position(latLng));
                    if(resName.equals(uname)) {
                        resMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.current_icon));
                        lokasyonko = resMarker.getPosition();
                    }
                    else if(!resName.equals(uname)) {
                        resMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.responder_icon));
                    }
                    resArrayMarker.add(resMarker);
                }//End for loop
                updateMarkerPosition(resArrayMarker,documentSnapshots.size());
            }//End onSuccess
        });//End CollectionReference
    }//End LoadResponderLocation

    private void updateMarkerPosition(final ArrayList<Marker> markerOptions, final int docSize) {

        responderReference.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {

                if (documentSnapshots.size()>docSize)
                {
                    mMap.clear();
                    loadEventLocation();
                    loadResponderLocation();
                }
                else {
                    for (int x = 0; x < documentSnapshots.size(); x++) {
                        resData = documentSnapshots.getDocuments().get(x).getData();
                        resLocation = ((GeoPoint) resData.get("location"));
                        resName = ((String) resData.get("username"));

                        LatLng latLng = new LatLng(resLocation.getLatitude(), resLocation.getLongitude());
                        markerOptions.get(x).setPosition(latLng);

                        if(resName.equals(uname)) {
                            lokasyonko = latLng;
                        }
                        if(reqCheck&&statCheck){
                            directionPositionList.clear();
                            polyline.remove();
                            requestDirection();
                        }

                    }//End for loop
                }
            }//End of onEvent
        });

    }

    private void loadEventLocation(){
        eventArrayMarker = new ArrayList<>();
        markerLoc = new ArrayList<>();
        eventReference.get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot documentSnapshots) {
                for(int x=0;x<documentSnapshots.size();x++) {
                    eventData = documentSnapshots.getDocuments().get(x).getData();
                    eventMapLocation=((GeoPoint)eventData.get("mapLocation"));
                    eventStatus=((String)eventData.get("status"));
                    eventType=((String)eventData.get("eventType"));
                    eventName=((String)eventData.get("eventName"));
                    eventLocation=((String)eventData.get("location"));
                    markerLoc.add(eventMapLocation);

                    LatLng latLng = new LatLng(eventMapLocation.getLatitude(),eventMapLocation.getLongitude());

                    eventMarker = mMap.addMarker(new MarkerOptions()
                            .title(eventType)
                            .snippet(eventLocation  + eventStatus)
                            .position(latLng));

                    if(eventType.equals("Fire Incident")) {
                        eventMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.fire_icon));

                    }
                    if(eventType.equals("Medical Emergency")) {
                        eventMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.medical_icon));
                        //<div>Icons made by <a href="https://www.flaticon.com/authors/smashicons" title="Smashicons">Smashicons</a>
                        // from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>
                        // is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">
                        // CC 3.0 BY</a></div>
                    }
                    if(eventType.equals("Vehicular Accident")) {
                        eventMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.crash_icon));
                        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                            @Override
                            public View getInfoWindow(Marker arg0) {
                                return null;
                            }

                            @RequiresApi(api = Build.VERSION_CODES.M)
                            @Override
                            public View getInfoContents(Marker marker) {

                                LinearLayout info = new LinearLayout(getContext());
                                info.setOrientation(LinearLayout.VERTICAL);


                                TextView title = new TextView(getContext());
                                title.setTextColor(Color.BLACK);
                                title.setGravity(Gravity.CENTER);
                                title.setTypeface(null, Typeface.BOLD);
                                title.setText(marker.getTitle());

                                TextView snippet = new TextView(getContext());
                                snippet.setTextColor(Color.GRAY);
                                snippet.setText(marker.getSnippet());

                                Button infoButton = new Button(getContext());

                                infoButton.setText("Direction");
                                infoButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                    }
                                });

                                info.addView(title);
                                info.addView(snippet);
                                info.addView(infoButton);

                                mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                                    @Override
                                    public void onInfoWindowClick(Marker marker) {

                                        directionButton.setVisibility(View.VISIBLE);
                                        destinasyon = marker.getPosition();
                                        reqCheck = true;
                                        polCheck = true;
                                        statCheck = true;
                                        requestDirection();
                                        Log.d("MAN","WATANAAYS");
                                    }
                                });

                                return info;
                            }
                        });

                    }
                    if(eventStatus.equals("Finished")){
                        eventMarker.setVisible(false);
                    }
                    eventArrayMarker.add(eventMarker);

                }//End for loop
                updateEventStatus(eventArrayMarker,documentSnapshots.size());
            }//End of Event
        });
    }

    private void updateEventStatus(final ArrayList<Marker> eventMarker, final int docSize){

        eventReference.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {

                if(documentSnapshots.size()>docSize){
                    mMap.clear();
                    loadResponderLocation();
                    loadEventLocation();
                }
                else {
                    for (int x = 0; x < documentSnapshots.size(); x++) {
                        eventData = documentSnapshots.getDocuments().get(x).getData();
                        eventLocation = ((String) eventData.get("location"));
                        eventName = ((String) eventData.get("username"));
                        eventStatus = ((String) eventData.get("status"));

                        // LatLng latLng = new LatLng(eventLocation.getLatitude(),eventLocation.getLongitude());

                        eventMarker.get(x).setSnippet(eventLocation + "\n" + eventStatus);

                    }//End for loop
                }
            }//End of onEvent
        });
    }

    private void displayLocation() {
        if(ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION)!= PackageManager.PERMISSION_GRANTED&&
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED) {
            return;
        }
        if(mLastLocation != null){
            //Update to Firebase
            double lat = (mLastLocation.getLatitude());
            double lng = (mLastLocation.getLongitude());
            geoPoint = new GeoPoint(lat, lng);

            //-0.0015407
            //+0.003435
            //  Toast.makeText(getContext(),"asd: "+lat,Toast.LENGTH_SHORT).show();
            Log.d("TEST","DisplayLocationNew: "+geoPoint);

            query1 = responderReference.whereEqualTo("username",uname);
            query1.get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                @Override
                public void onSuccess(QuerySnapshot documentSnapshots) {
                    docID = documentSnapshots.getDocuments().get(0).getId();
                    //     Toast.makeText(getContext(), docId,Toast.LENGTH_SHORT).show();
                    documentReference.getParent().document(docID).update("location",geoPoint);
                }
            });

        }
        else {

            Log.d("TEST","Couldn't load location");
        }
    }//End of Display Locations

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setSmallestDisplacement(DISTANCE);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getContext()).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();

        Log.d("APIC","RESULT: "+  mGoogleApiClient.isConnected());
    }

    private boolean checkPlayService() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getContext());
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, getActivity(), PLAY_SERVICES_RES_REQUEST).show();
            } else {
                Toast.makeText(getContext(), "This device is not supported", Toast.LENGTH_SHORT).show();
                getActivity().finish();
            }
            return false;
        }
        return true;
    }

    private void setupSystemNew(){

        query1 = responderReference.whereEqualTo("username",uname);
        query1.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {

            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                QuerySnapshot qq = task.getResult();
                docID =qq.getDocuments().get(0).getId();

                Log.d("FOO",docID);


                responderReference.document(docID).addSnapshotListener(new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(DocumentSnapshot documentSnapshot, FirebaseFirestoreException e) {
                        responderReference.document(docID).update("status","Available");
                    }
                });
            }



        });

//        responderFireReference = FirebaseDatabase.getInstance().getReference("Responders");
//        currentUserRef = FirebaseDatabase.getInstance().getReference("Responders").child("Responder");//Create new child in listOnline with key is uid


        query1.get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot documentSnapshots) {
                docID = documentSnapshots.getDocuments().get(0).getId();

                responderFireReference.addValueEventListener(new ValueEventListener() {

                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        responderFireReference.onDisconnect().setValue("status","Offline");

                        Log.d("WATUB:","HAHAHA>>>"+docID+"<<<HAHAHA");

                        //Responder Id dapat yung nsa child V
                        responderFireReference.child(docID)
                                .setValue(new Status("Online"));

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }

                });//End of responderFireReference


            }
        });


    }//End of system setup

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode)
        {
            case MY_PERMISSION_REQUEST_CODE:
            {
                if(grantResults.length>0&&grantResults[0]==PackageManager.PERMISSION_GRANTED)
                {
                    if(checkPlayService())
                    {
                        buildGoogleApiClient();
                        createLocationRequest();
                        displayLocation();
                    }

                }

            }

        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        if(ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION)!= PackageManager.PERMISSION_GRANTED&&
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED)
        {
            return;
        }

        //get latlong for corners for specified city

        LatLng one = new LatLng(14.238168, 121.04297);
        LatLng two = new LatLng(14.352614, 121.116427);

        //Paltan mo to ^ nag ffixed size pag nag zoom

        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        //add them to builder
        builder.include(one);
        builder.include(two);

        LatLngBounds bounds = builder.build();

        //get width and height to current display screen
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;

        // 20% padding
        int padding = (int) (width * 0.20);

        //set latlong bounds
        mMap.setLatLngBoundsForCameraTarget(bounds);

        //move camera to fill the bound to screen
        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding));

        //set zoom to level to current so that you won't be able to zoom out viz. move outside bounds
        mMap.setMinZoomPreference(mMap.getCameraPosition().zoom);

        mMap.setMyLocationEnabled(true);

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        displayLocation();
        checkEventsNearby(location);
    }

    private void checkEventsNearby(Location loc) {

        for (int x = 0; x < markerLoc.size(); x++) {


            Location mLoc = new Location("");
            //markerLoc.get(x).getLatitude(),markerLoc.get(x).getLongitude()


            mLoc.setLatitude(markerLoc.get(x).getLatitude());
            mLoc.setLongitude(markerLoc.get(x).getLongitude());

            Log.d("EventsLocationDistance",""+loc.distanceTo(mLoc));

            if(loc.distanceTo(mLoc)<100&&eventArrayMarker.get(x).getSnippet().contains("Ongoing")){
                builder.setTitle("Event within 100 meters!")
                        // .setMessage("Are you sure you want to delete this entry?")
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                

                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();

            }

        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        displayLocation();
        startLocationUpdates();
    }

    private void startLocationUpdates() {
        if(ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION)!= PackageManager.PERMISSION_GRANTED&&
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED)
        {   return;}
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,mLocationRequest,  this);

    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("PICA","Connection Failed");
        Toast.makeText(getContext(), "Connection Failed",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStart() {
        super.onStart();

        if(mGoogleApiClient != null){
            mGoogleApiClient.connect();
            Log.d("PICA","Successfully connected to ApiClient");
        }
        else{
            Log.d("PICA","Failed to connect ApiClient");
            //   Toast.makeText(getContext(), "Failed to connect ApiClient", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onStop() {
        if(mGoogleApiClient !=null){

//            counterRef.child(FirebaseAuth.getInstance().getCurrentUser().getUid())
//                    .setValue(new Status(FirebaseAuth.getInstance().getCurrentUser().getEmail(),"Offline"));
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("BAZ","DANKE");

    }

    @Override
    public void onResume() {
        super.onResume();
//        counterRef.child(FirebaseAuth.getInstance().getCurrentUser().getUid())
//                .setValue(new Status(FirebaseAuth.getInstance().getCurrentUser().getEmail(),"Online"));

        checkPlayService();
    }

    @Override
    public void onDirectionSuccess(Direction direction, String rawBody) {

        if (direction.isOK()) {
            Route route = direction.getRouteList().get(0);
            for(int x=0;x<directionPositionList.size();x++) {
                polyline.remove();
            }
            directionPositionList.clear();
            directionPositionList = route.getLegList().get(0).getDirectionPoint();
            // polyline = DirectionConverter.createPolyline(getContext(), directionPositionList, 5, Color.RED);
            polyline = mMap.addPolyline(DirectionConverter.createPolyline(getContext(), directionPositionList, 5, Color.CYAN));
            setCameraWithCoordinationBounds(route);
            onDirectionComplete(polyline,directionPositionList.size());

            if(toastCheck) {
                Toast.makeText(getContext(), "Direction added", Toast.LENGTH_SHORT).show();
                toastCheck=false;
            }
//            btnRequestDirection.setVisibility(View.GONE);
        }
        else {
            Log.d(TAG,direction.getStatus());
        }
        directionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                polyline.remove();

                Toast.makeText(getContext(),"Direction removed",Toast.LENGTH_SHORT).show();
                reqCheck = false;
                toastCheck = true;
                directionButton.setVisibility(View.INVISIBLE);
            }
        });

    }

    public void onDirectionComplete(final Polyline poly, final int polSize){
        if(poly.getPoints().size()<7&&polCheck){
            polCheck=false;
            builder.setTitle("Event Ahead!")
                    // .setMessage("Are you sure you want to delete this entry?")
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            statCheck=false;

                            //ADD  ka pa ng clear direction button
                            poly.remove();
                            for(int x=0;x<polSize;x++) {
                                poly.remove();
                            }
                        }
                    })
//                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int which) {
//                            // do nothing
//                        }
//                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
    }

    @Override
    public void onDirectionFailure(Throwable t) {

        Log.d(TAG,t.getMessage());

    }

    private void setCameraWithCoordinationBounds(Route route) {
        LatLng southwest = route.getBound().getSouthwestCoordination().getCoordination();
        LatLng northeast = route.getBound().getNortheastCoordination().getCoordination();
        LatLngBounds bounds = new LatLngBounds(southwest, northeast);
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
    }
}//End of Class