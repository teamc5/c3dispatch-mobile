package com.example.arvin.c3dispatch.RecyclerViewThings;

/**
 * Created by Arvin on 17 Mar 2018.
 */

public class Audio  {
    private String mAudName;
    private String mAudUrl;

    public String getmAudUrl() {
        return mAudUrl;
    }

    public void setmAudUrl(String mAudUrl) {
        this.mAudUrl = mAudUrl;
    }

    public String getmAudName() {

        return mAudName;
    }

    public void setmAudName(String mAudName) {
        this.mAudName = mAudName;
    }
}