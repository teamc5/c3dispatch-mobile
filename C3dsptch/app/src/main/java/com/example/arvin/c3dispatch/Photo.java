package com.example.arvin.c3dispatch;

import java.util.Date;

/**
 * Created by gbbarredo on 4/18/2018.
 */

public class Photo {

    private String mUrl;

    public Photo() {

    }

    public Photo(String Url) {
        mUrl = Url;
    }


    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String mImgUrl) {
        this.mUrl = mImgUrl;
    }

}
