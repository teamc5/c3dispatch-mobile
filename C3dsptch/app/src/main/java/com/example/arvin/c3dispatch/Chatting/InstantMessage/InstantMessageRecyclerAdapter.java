package com.example.arvin.c3dispatch.Chatting.InstantMessage;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.arvin.c3dispatch.R;

import java.util.List;

/**
 * Created by Aaron on 12/04/2018.
 */

public class InstantMessageRecyclerAdapter extends RecyclerView.Adapter<InstantMessageHolder> {

    private List<InstantMessage> instantMessageList;
    private OnItemClicked onClick;

    public InstantMessageRecyclerAdapter(List<InstantMessage> instantMessageList) {
        this.instantMessageList = instantMessageList;
    }

    @Override
    public InstantMessageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.popup_instant_message_item,parent,false);
        return new InstantMessageHolder(itemView);
    }

    @Override
    public void onBindViewHolder(InstantMessageHolder holder, final int position) {
        // loload yung strings tas ilalagay sa bind isa isa

        InstantMessage im = instantMessageList.get(position);
        holder.bind(im.getText());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClick.onItemClick(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return instantMessageList.size();
    }

    public interface OnItemClicked {
        void onItemClick(int position);
    }

    public void setOnClick(OnItemClicked onClick) {
        this.onClick=onClick;
    }

    public InstantMessage getItem(int position){
        return instantMessageList.get(position);
    }
}
