package com.example.arvin.c3dispatch;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.arvin.c3dispatch.Chatting.FirestoreChatActivity;
import com.example.arvin.c3dispatch.RecyclerViewThings.Event;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

/**
 * Created by Aaron on 22/03/2018.
 */

public class MyPushNotifications {


    private CollectionReference eventCollectionReference;
    private Query query;
    private ValContainer<String> responderDocId;
    //private Query eventQuery;
    private DocumentReference eventDocRef;
    private ValContainer<Event> myEvent;


    private Event thisEvent;

    private String myId;
    private String refString;





    private final String TAG = "MyPushNotications ";




    public void SendRespondNotificationToServer(final Context context, final FirebaseAuth mAuth, final String docId) {


        // gawa pa ko ng isa pang function tas dun ko ilalgay yung pag send
        // tas tsaka ko ilalagay sa on succcess listener

        responderDocId = new ValContainer<>();
        myEvent = new ValContainer<>();
        Log.d("Debug", docId);

        //eventThis = myEvent.getVal();

        eventCollectionReference = FirebaseFirestore.getInstance().collection("Events").document(docId).collection("Responders");
        query = FirebaseFirestore.getInstance().collection("Responders").whereEqualTo("username", mAuth.getUid());
        query.get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot documentSnapshots) {

                setResponderId(documentSnapshots.getDocuments().get(0).getId());
                Log.d("RespnderId: ", responderDocId.getVal());
               // Log.d("EventName", thisEvent.getEventName());
                NotifyResponse(mAuth, context);
                //Log.d(myEvent)




            }

            public void setResponderId(String id) {
                responderDocId.setVal(id);

            }

        });


        //String refString = "/Responders/"+responderDocId.getVal();
        //String refString = "/Responders/"+responderDocId.getVal();

        Log.d("RefString", "ASda" + refString);


    }

    public void GetEvent(String docId){
        eventDocRef = FirebaseFirestore.getInstance().collection("Events").document(docId);
        Log.d("Debug", "dumaan sa get Event");
        eventDocRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                // myEvent.setVal(documentSnapshot.toObject(Event.class));
//                Event event = documentSnapshot.toObject(Event.class);
//                myEvent.setVal(event);

                if(documentSnapshot.exists()){
                    thisEvent = documentSnapshot.toObject(Event.class);

                    Log.d("Value container: ", thisEvent.getEventId());
                }else{
                    Log.d("Document", "dost not exist");
                }




            }
        }).addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()){
                    DocumentSnapshot document = task.getResult();
                    if (document != null && document.exists()) {
                        thisEvent = document.toObject(Event.class);
                        Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                    } else {
                        Log.d(TAG, "No such document");
                    }
                }else {
                    Log.d(TAG, "get failed with ", task.getException());
                }
            }
        });

    }

    private void NotifyResponse(final FirebaseAuth mAuth, final Context context) {

        try {
            String url = "http://c3dispatch-env.us-east-2.elasticbeanstalk.com/api/sendnotif";
            //String url = "http://192.168.0.105:80/dispatch2/public/api/sendpush";
            RequestQueue requestQueue = Volley.newRequestQueue(context);

            // eventThis = myEvent.getVal();

            // put a flag here thread

//            Log.d("EventThis: ", myEvent.getVal().getEventId()+" Start");

            // di ko alam kung pano ko ilalagayh yung location
            JSONObject jsonBody = new JSONObject();

            jsonBody.put("title", thisEvent.getEventName());

            //jsonBody.put("title", "test");
            jsonBody.put("message", mAuth.getUid() + " has responded to the incident that occurred at " + thisEvent.getLocation());
            //jsonBody.put("message", mAuth.getUid() + " has responded to the incident that occurred at ");
            jsonBody.put("type", "respond");
            jsonBody.put("sender", mAuth.getUid());
            jsonBody.put("channel_name", "cmd_center_respond");



            final String requestBody = jsonBody.toString();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {


                    //Toast.makeText(getApplicationContext(), "notif not sent", Toast.LENGTH_LONG).show();
                    Log.i("VOLLEY", response);


                    // i add as reference sa Events.Responder
                    HashMap<String, String> responderMap = new HashMap<>();
                    Log.d("RespnderId: ", responderDocId.getVal());
                    refString = "/Responders/" + responderDocId.getVal();
                    responderMap.put("id", refString);
                    Log.d("ResponderMap: ", responderMap.toString());
                    //Log.d("JsonBody", requestBody)
                    eventCollectionReference.add(responderMap).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.e(TAG, "Failed to write the reference", e);

                        }
                    });

                    Log.d("RespnderId: ", responderDocId.getVal() + " END");

                    //

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
//
                }
            }) {


                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

               /* @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json");
                    return params;
                }*/
            };

            requestQueue.add(stringRequest);


        } catch (JSONException e) {

        }
    }


    public static void NotifyReportSubmitted(Context context, FirebaseAuth mAuth, String eventId, String type){


        try {
            String url = "http://c3dispatch-env.us-east-2.elasticbeanstalk.com/api/sendnotif";
            //String url = "http://192.168.0.105:80/dispatch2/public/api/sendpush";
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("title", type+" submitted for " + eventId);
            jsonBody.put("message", mAuth.getUid() + " submitted " + type );
            //jsonBody.put("type", "message");
            jsonBody.put("type", "report");
            jsonBody.put("sender", mAuth.getUid());
            jsonBody.put("channel_name", "cmd_center_report");
            //jsonBody.put("channel_name", "cmd_center");
            final String requestBody = jsonBody.toString();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {


                    //Toast.makeText(getApplicationContext(), "notif not sent", Toast.LENGTH_LONG).show();
                    Log.i("VOLLEY", response);
                    // Toast.makeText(FirestoreChatActivity.this, response, Toast.LENGTH_SHORT).show();
                    //

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
//
                    //Toast.makeText(FirestoreChatActivity.this, "failed to send", Toast.LENGTH_SHORT).show();
                }
            }) {


                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

               /* @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json");
                    return params;
                }*/
            };

            requestQueue.add(stringRequest);


        } catch (JSONException e) {

        }

    }



    public class ValContainer<T> {
        private T val;

        public ValContainer() {
        }

        public ValContainer(T v) {
            this.val = v;
        }

        public T getVal() {
            return val;
        }

        public void setVal(T val) {
            this.val = val;
        }
    }

}
