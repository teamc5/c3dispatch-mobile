package com.example.arvin.c3dispatch.RecyclerViewThings;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.arvin.c3dispatch.R;
//import com.c3.aaron.c3chat.R;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Aaron on 23/02/2018.
 */

public class EventHolder extends RecyclerView.ViewHolder {


    private CircleImageView mImage;
    private TextView mTimeStamp;
    private RelativeLayout mRelativeLayout;
    private TextView mEventTitle;
    private TextView mLastMessage;

    public EventHolder(View itemView) {
        super(itemView);
        mImage = itemView.findViewById(R.id.event_list_thumbnail);
        mTimeStamp = itemView.findViewById(R.id.event_list_time_stamp);
        mRelativeLayout = itemView.findViewById(R.id.event_list_text_layout);
        mEventTitle = itemView.findViewById(R.id.event_list_title);
        mLastMessage = itemView.findViewById(R.id.event_list_subtitle);
    }

    public void bind(AbstractEvent event) {
        setEventText(event.getLocation());
        setStatus(event.getTimeOfCall().toString());
        setImage(event.getEventType());

/*        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        setIsSender(currentUser != null && chat.getUid().equals(currentUser.getUid()));*/
    }
    //mNameField.setText(name)
    private void setEventText(String text){
        mEventTitle.setText(text);
    }

    private void setStatus(String text){
        mLastMessage.setText(text);
    }

    // replace yung image depende dun sa eventType
    private void setImage(String type){

        switch(type) {
            case "Fire Incident":
                Picasso.get().load(R.drawable.fire).placeholder(R.mipmap.ic_launcher).into(mImage);
                break;
            case "Medical Emergency":
                Picasso.get().load(R.drawable.medical).placeholder(R.mipmap.ic_launcher).into(mImage);
                break;
            case "Vehicular Accident":
                Picasso.get().load(R.drawable.vehicle_accident).placeholder(R.mipmap.ic_launcher).into(mImage);
                break;
        }
        //Picasso.with(mImage.getContext()).load(R.drawable.face_1).placeholder(R.mipmap.ic_launcher).into(mImage);
    }


}
