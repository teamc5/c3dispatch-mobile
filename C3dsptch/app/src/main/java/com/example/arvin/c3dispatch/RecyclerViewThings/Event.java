package com.example.arvin.c3dispatch.RecyclerViewThings;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.ServerTimestamp;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Aaron on 20/02/2018.
 */

public class Event extends AbstractEvent implements Parcelable{

    private String eventId;
    private String eventName;
    private String eventType;
    private String location;
    private GeoPoint mapLocation;
    private String status;
    private Date timeOfCall;

    public Event() {
    }

    public Event(String eventId) {
        this.eventId = eventId;
    }

    protected Event(Parcel in) {
        eventId = in.readString();
        eventName = in.readString();
        eventType = in.readString();
        location = in.readString();
        status = in.readString();
    }

    public static final Creator<Event> CREATOR = new Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel in) {
            return new Event(in);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public GeoPoint getMapLocation() {
        return mapLocation;
    }

    public void setMapLocation(GeoPoint mapLocation) {
        this.mapLocation = mapLocation;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @ServerTimestamp
    public Date getTimeOfCall() {
        return timeOfCall;
    }

    public void setTimeOfCall(Date timeOfCall) {
        this.timeOfCall = timeOfCall;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(eventId);
        parcel.writeString(eventName);
        parcel.writeString(eventType);
        parcel.writeString(location);
        parcel.writeString(status);
    }



    @Override
    public boolean equals(Object v) {
        boolean retVal = false;

        if (v instanceof Event){
            Event ptr = (Event) v;
            retVal = ptr.eventId == this.eventId;
        }

        return retVal;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + (this.eventId != null ? this.eventId.hashCode() : 0);
        return hash;
    }
}
