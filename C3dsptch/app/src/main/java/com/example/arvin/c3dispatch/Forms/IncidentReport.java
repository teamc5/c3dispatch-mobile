package com.example.arvin.c3dispatch.Forms;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;


import com.example.arvin.c3dispatch.Forms.Gallery.GalleryActivity;
import com.example.arvin.c3dispatch.Forms.Gallery.GalleryListenerStarter;
import com.example.arvin.c3dispatch.Forms.Models.IncidentReportModel;
import com.example.arvin.c3dispatch.MyPushNotifications;
import com.example.arvin.c3dispatch.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import me.riddhimanadib.formmaster.FormBuilder;
import me.riddhimanadib.formmaster.listener.OnFormElementValueChangedListener;
import me.riddhimanadib.formmaster.model.BaseFormElement;
import me.riddhimanadib.formmaster.model.FormElementPickerDate;
import me.riddhimanadib.formmaster.model.FormElementPickerSingle;
import me.riddhimanadib.formmaster.model.FormElementPickerTime;
import me.riddhimanadib.formmaster.model.FormElementTextMultiLine;
import me.riddhimanadib.formmaster.model.FormElementTextNumber;
import me.riddhimanadib.formmaster.model.FormElementTextSingleLine;
import me.riddhimanadib.formmaster.model.FormHeader;


public class IncidentReport extends AppCompatActivity {

    private static final String TAG ="IncidentReport" ;
    private RecyclerView mRecyclerView;
    private FormBuilder mFormBuilder;
    private Button mBtnSubmit;
    private List<BaseFormElement> formItems;
    private List<String> formLabels;
    private FirebaseFirestore db;
    private DialogInterface.OnClickListener dialogClickListener;
    private String eventId;
    private FirebaseAuth mAuth;
    private IncidentReportModel incidentReportModel;
    private List<String> photos;
    private Date date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incident_report);
        SetupForm();



        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                eventId = null;
            } else {
                eventId = extras.getString("eventId");
                Date dateObj = new Date();




                Log.d("TimeNgMamaMo", dateObj.toString());


            }

        }
        GalleryListenerStarter.StartListening(eventId);
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle((Html.fromHtml("<font color=\"#FFFFFF\">" + getString(R.string.incident_report) + "</font>")));


        dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        //Yes button clicked
                        SubmitForm();
                        MyPushNotifications.NotifyReportSubmitted(getApplicationContext(), mAuth, eventId, "Incident Report");
                        finish();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };



/*
        // can be converted into an object
        formLabels = new ArrayList<>();
        formLabels.add("Header1");
        formLabels.add("Date");
        formLabels.add("Time of Call");
        formLabels.add("Call Receiver");
        formLabels.add("Time of Arrival at the location");
        formLabels.add("Location");
        formLabels.add("Number of Patient/s");
        formLabels.add("Header2");
        formLabels.add("Name/s");
        formLabels.add("Address");
        formLabels.add("Gender");
        formLabels.add("Age");
        formLabels.add("Initial Assessment");
        formLabels.add("Responding Team/s");
        formLabels.add("Prepared by");
*/







      //          StringBuilder sb = new StringBuilder();
        //        for(BaseFormElement b : formItems){
          //          sb.append(b.getValue()+ "\n");
            //    }
              //  Toast.makeText(IncidentReport.this, sb.toString(), Toast.LENGTH_LONG).show();



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_with_send, menu);



        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        if (id == R.id.action_send) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Are you sure you have filled out all the fields correctly?").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();
            // Do something
            return true;
        }else if (id == R.id.action_attach_photos){

            Intent intent = new Intent(getApplicationContext(), GalleryActivity.class);

            startActivityForResult(intent, RequestCode.SELECT_PHOTOS);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void SetupForm(){
        // initialize variables
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mFormBuilder = new FormBuilder(this, mRecyclerView, new OnFormElementValueChangedListener() {
            @Override
            public void onValueChanged(BaseFormElement formElement) {
                //Toast.makeText(IncidentReport.this, formElement.getValue(), Toast.LENGTH_SHORT).show();
            }
        });


        // setup strings for gender
        List<String> genderString = new ArrayList<>();
        genderString.add("Male");
        genderString.add("Female");

        // declare form elements
        // first part of form
        FormHeader header = FormHeader.createInstance("Incident Report");
        FormElementPickerDate date = FormElementPickerDate.createInstance().setTitle("Date").setDateFormat("MMM dd, yyyy").setHint("Date");
        FormElementPickerTime timeOfCall = FormElementPickerTime.createInstance().setTitle("Time of Call").setHint("Time of call");
        //timeOfCall.setValue(time);
        FormElementTextSingleLine callReceiver = FormElementTextSingleLine.createInstance().setTitle("Call Receiver").setHint("Call Receiver");
        FormElementPickerTime toaLocation = FormElementPickerTime.createInstance().setTitle("Time of Arrival at the location").setHint("Time of arrival at location");
        FormElementTextSingleLine location = FormElementTextSingleLine.createInstance().setTitle("Location").setHint("Location");
        FormElementTextNumber numPatients = FormElementTextNumber.createInstance().setTitle("Number of Patient/s").setHint("Number of Patients");

        // second part of form
        FormHeader header2 = FormHeader.createInstance("Patient/s Profile");
        FormElementTextMultiLine patientName = FormElementTextMultiLine.createInstance().setTitle("Name/s").setHint("Patient Name");
        FormElementTextMultiLine patientAddress = FormElementTextMultiLine.createInstance().setTitle("Address").setHint("Patient Address");
        FormElementPickerSingle patientGender = FormElementPickerSingle.createInstance().setTitle("Gender").setHint("Gender");
        patientGender.setOptions(genderString);
        FormElementTextNumber patientAge = FormElementTextNumber.createInstance().setTitle("Age").setHint("Age");
        FormElementTextMultiLine patientIniAssessment = FormElementTextMultiLine.createInstance().setTitle("Initial Assessment").setHint("Initial Assessment");
        FormElementTextMultiLine respondingTeams = FormElementTextMultiLine.createInstance().setTitle("Responding Team/s").setHint("Responding Teams");
        FormElementTextSingleLine prepBy = FormElementTextSingleLine.createInstance().setTitle("Prepared by").setHint("Prepared By");



        // add them in a list
        formItems = new ArrayList<>();
        formItems.add(header);
        formItems.add(date);
        formItems.add(timeOfCall);
        formItems.add(callReceiver);
        formItems.add(toaLocation);
        formItems.add(location);
        formItems.add(numPatients);

        formItems.add(header2);
        formItems.add(patientName);
        formItems.add(patientAddress);
        formItems.add(patientGender);
        formItems.add(patientAge);
        formItems.add(patientIniAssessment);
        formItems.add(respondingTeams);
        formItems.add(prepBy);




        // build and display the form
        mFormBuilder.addFormElements(formItems);
    }
    public void SubmitForm(){
        incidentReportModel = new IncidentReportModel(formItems.get(1).getValue(),
        formItems.get(2).getValue(),
        formItems.get(3).getValue(),
        formItems.get(4).getValue(),
        formItems.get(5).getValue(),
        formItems.get(6).getValue(),
        formItems.get(8).getValue(),
        formItems.get(9).getValue(),
        formItems.get(10).getValue(),
        formItems.get(11).getValue(),
        formItems.get(12).getValue(),
        formItems.get(13).getValue(),
        formItems.get(14).getValue());

        incidentReportModel.setPhotos(photos);


        db.collection("Events")
        .document(eventId)
        .collection("Incident Reports")
                .add(incidentReportModel).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {

            @Override
            public void onSuccess(DocumentReference documentReference) {
                Log.d(TAG, "Successfully submitted");
            }
        });
        // this line should be the eventId it is connected to
        // since originally nasa loob naman to ng event mismo makukuha yung id
        // i q-quyeueury lang nice(y)



        finishActivity(0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == RequestCode.SELECT_PHOTOS){
            if(resultCode == RESULT_OK){
                // ilagay sa model yung photos

               photos = data.getStringArrayListExtra("photos");
            }
        }
    }

}
