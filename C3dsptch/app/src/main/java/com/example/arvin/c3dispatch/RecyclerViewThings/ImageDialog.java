package com.example.arvin.c3dispatch.RecyclerViewThings;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.arvin.c3dispatch.R;
import com.squareup.picasso.Picasso;

/**
 * Created by gbbarredo on 4/3/2018.
 */

public class ImageDialog extends DialogFragment {
    private ImageView mimgDialogView;
    private String imgUrl;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_image_dialog, container, false);
        mimgDialogView = view.findViewById(R.id.imgDialog);

        Bundle bundle = getArguments();

        imgUrl = bundle.getString("url");
        //Toast.makeText(getActivity(), imgUrl, Toast.LENGTH_LONG).show();
        //Toast.makeText(getContext(), imgUrl, Toast.LENGTH_SHORT).show();
        Picasso.get().load(imgUrl).placeholder(R.drawable.circle).into(mimgDialogView);


        return view;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if(getActivity()!= null)
            getActivity().finish();
        super.onDismiss(dialog);

    }
}
