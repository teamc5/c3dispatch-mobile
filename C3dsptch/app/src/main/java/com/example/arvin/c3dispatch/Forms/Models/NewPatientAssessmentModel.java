package com.example.arvin.c3dispatch.Forms.Models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by Aaron on 02/04/2018.
 */

public class NewPatientAssessmentModel implements Serializable{
    private String allergies;
    private String breathing1;
    private String breathing2;
    private String breathing3;
    private String conscious1;
    private String conscious2;
    private String conscious3;
    private String date;
    private String discharge;
    private String doctorSignature;
    private String firstAider;
    private String location;
    private String medications;
    private String otherObservation1;
    private String otherObservation2;
    private String otherObservation3;
    private String pastMedicalHistory;
    private String patientAddress;
    private String patientDOB;
    private String patientFamilyName;
    private String patientGivenName;
    private String patientSex;
    private String patientTelephone;
    private String pulse1;
    private String pulse2;
    private String pulse3;
    private String refustTreatment;
    private String time;
    private String time1;
    private String time2;
    private String time3;
    private String timeOut;
    private String whatHappened;
    private String witnessFamilyName;
    private String witnessGivenName;
    private String witnessTelephone;
    private List<String> photos;

    public String getAllergies() {
        return allergies;
    }

    public void setAllergies(String allergies) {
        this.allergies = allergies;
    }

    public String getBreathing1() {
        return breathing1;
    }

    public void setBreathing1(String breathing1) {
        this.breathing1 = breathing1;
    }

    public String getBreathing2() {
        return breathing2;
    }

    public void setBreathing2(String breathing2) {
        this.breathing2 = breathing2;
    }

    public String getBreathing3() {
        return breathing3;
    }

    public void setBreathing3(String breathing3) {
        this.breathing3 = breathing3;
    }

    public String getConscious1() {
        return conscious1;
    }

    public void setConscious1(String conscious1) {
        this.conscious1 = conscious1;
    }

    public String getConscious2() {
        return conscious2;
    }

    public void setConscious2(String conscious2) {
        this.conscious2 = conscious2;
    }

    public String getConscious3() {
        return conscious3;
    }

    public void setConscious3(String conscious3) {
        this.conscious3 = conscious3;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDischarge() {
        return discharge;
    }

    public void setDischarge(String discharge) {
        this.discharge = discharge;
    }

    public String getDoctorSignature() {
        return doctorSignature;
    }

    public void setDoctorSignature(String doctorSignature) {
        this.doctorSignature = doctorSignature;
    }

    public String getFirstAider() {
        return firstAider;
    }

    public void setFirstAider(String firstAider) {
        this.firstAider = firstAider;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getMedications() {
        return medications;
    }

    public void setMedications(String medications) {
        this.medications = medications;
    }

    public String getOtherObservation1() {
        return otherObservation1;
    }

    public void setOtherObservation1(String otherObservation1) {
        this.otherObservation1 = otherObservation1;
    }

    public String getOtherObservation2() {
        return otherObservation2;
    }

    public void setOtherObservation2(String otherObservation2) {
        this.otherObservation2 = otherObservation2;
    }

    public String getOtherObservation3() {
        return otherObservation3;
    }

    public void setOtherObservation3(String otherObservation3) {
        this.otherObservation3 = otherObservation3;
    }

    public String getPastMedicalHistory() {
        return pastMedicalHistory;
    }

    public void setPastMedicalHistory(String pastMedicalHistory) {
        this.pastMedicalHistory = pastMedicalHistory;
    }

    public String getPatientAddress() {
        return patientAddress;
    }

    public void setPatientAddress(String patientAddress) {
        this.patientAddress = patientAddress;
    }

    public String getPatientDOB() {
        return patientDOB;
    }

    public void setPatientDOB(String patientDOB) {
        this.patientDOB = patientDOB;
    }

    public String getPatientFamilyName() {
        return patientFamilyName;
    }

    public void setPatientFamilyName(String patientFamilyName) {
        this.patientFamilyName = patientFamilyName;
    }

    public String getPatientGivenName() {
        return patientGivenName;
    }

    public void setPatientGivenName(String patientGivenName) {
        this.patientGivenName = patientGivenName;
    }

    public String getPatientSex() {
        return patientSex;
    }

    public void setPatientSex(String patientSex) {
        this.patientSex = patientSex;
    }

    public String getPatientTelephone() {
        return patientTelephone;
    }

    public void setPatientTelephone(String patientTelephone) {
        this.patientTelephone = patientTelephone;
    }

    public String getPulse1() {
        return pulse1;
    }

    public void setPulse1(String pulse1) {
        this.pulse1 = pulse1;
    }

    public String getPulse2() {
        return pulse2;
    }

    public void setPulse2(String pulse2) {
        this.pulse2 = pulse2;
    }

    public String getPulse3() {
        return pulse3;
    }

    public void setPulse3(String pulse3) {
        this.pulse3 = pulse3;
    }

    public String getRefustTreatment() {
        return refustTreatment;
    }

    public void setRefustTreatment(String refustTreatment) {
        this.refustTreatment = refustTreatment;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTime1() {
        return time1;
    }

    public void setTime1(String time1) {
        this.time1 = time1;
    }

    public String getTime2() {
        return time2;
    }

    public void setTime2(String time2) {
        this.time2 = time2;
    }

    public String getTime3() {
        return time3;
    }

    public void setTime3(String time3) {
        this.time3 = time3;
    }

    public String getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(String timeOut) {
        this.timeOut = timeOut;
    }

    public String getWhatHappened() {
        return whatHappened;
    }

    public void setWhatHappened(String whatHappened) {
        this.whatHappened = whatHappened;
    }

    public String getWitnessFamilyName() {
        return witnessFamilyName;
    }

    public void setWitnessFamilyName(String witnessFamilyName) {
        this.witnessFamilyName = witnessFamilyName;
    }

    public String getWitnessGivenName() {
        return witnessGivenName;
    }

    public void setWitnessGivenName(String witnessGivenName) {
        this.witnessGivenName = witnessGivenName;
    }

    public String getWitnessTelephone() {
        return witnessTelephone;
    }

    public void setWitnessTelephone(String witnessTelephone) {
        this.witnessTelephone = witnessTelephone;
    }

    public List<String> getPhotos() {
        return photos;
    }

    public void setPhotos(List<String> photos) {
        this.photos = photos;
    }
}
