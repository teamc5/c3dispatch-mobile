package com.example.arvin.c3dispatch;

/**
 * Created by Aaron on 19/02/2018.
 */

public abstract class AbstractChat {

    public abstract String getSender();

    public abstract String getMessage();

    public abstract String getUrl();

    public abstract String getType();

    public abstract String getUid();

    @Override
    public abstract int hashCode();

    @Override
    public abstract boolean equals(Object obj);

}