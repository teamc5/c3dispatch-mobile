package com.example.arvin.c3dispatch.Forms.Models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Aaron on 13/02/2018.
 */

public class IncidentReportModel implements Serializable{
    private String date;
    private String incidentLocation;
    private String numOfPatient;
    private String patientAddress;
    private String patientAge;
    private String patientGender;
    private String patientIniAssess;
    private String patientName;
    private String preparedBy;
    private String respondTeam;
    private String timeOfArrival;
    private String timeOfCall;

    private String callReceiver;
    private List<String> photos;









    public IncidentReportModel(String date, String timeOfCall, String callReceiver, String timeOfArrival, String incidentLocation, String numOfPatient, String patientName, String patientAddress, String patientGender, String patientAge, String patientIniAssess, String respondTeam, String preparedBy) {
        this.date = date;
        this.timeOfCall = timeOfCall;
        this.callReceiver = callReceiver;
        this.timeOfArrival = timeOfArrival;
        this.incidentLocation = incidentLocation;
        this.numOfPatient = numOfPatient;
        this.patientName = patientName;
        this.patientAddress = patientAddress;
        this.patientGender = patientGender;
        this.patientAge = patientAge;
        this.patientIniAssess = patientIniAssess;
        this.respondTeam = respondTeam;
        this.preparedBy = preparedBy;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTimeOfCall() {
        return timeOfCall;
    }

    public void setTimeOfCall(String timeOfCall) {
        this.timeOfCall = timeOfCall;
    }

    public String getCallReceiver() {
        return callReceiver;
    }

    public void setCallReceiver(String callReceiver) {
        this.callReceiver = callReceiver;
    }

    public String getTimeOfArrival() {
        return timeOfArrival;
    }

    public void setTimeOfArrival(String timeOfArrival) {
        this.timeOfArrival = timeOfArrival;
    }

    public String getIncidentLocation() {
        return incidentLocation;
    }

    public void setIncidentLocation(String incidentLocation) {
        this.incidentLocation = incidentLocation;
    }

    public String getNumOfPatient() {
        return numOfPatient;
    }

    public void setNumOfPatient(String numOfPatient) {
        this.numOfPatient = numOfPatient;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientAddress() {
        return patientAddress;
    }

    public void setPatientAddress(String patientAddress) {
        this.patientAddress = patientAddress;
    }

    public String getPatientGender() {
        return patientGender;
    }

    public void setPatientGender(String patientGender) {
        this.patientGender = patientGender;
    }

    public String getPatientAge() {
        return patientAge;
    }

    public void setPatientAge(String patientAge) {
        this.patientAge = patientAge;
    }

    public String getPatientIniAssess() {
        return patientIniAssess;
    }

    public void setPatientIniAssess(String patientIniAssess) {
        this.patientIniAssess = patientIniAssess;
    }

    public String getRespondTeam() {
        return respondTeam;
    }

    public void setRespondTeam(String respondTeam) {
        this.respondTeam = respondTeam;
    }

    public String getPreparedBy() {
        return preparedBy;
    }

    public void setPreparedBy(String preparedBy) {
        this.preparedBy = preparedBy;
    }

    public List<String> getPhotos() {
        return photos;
    }

    public void setPhotos(List<String> photos) {
        this.photos = photos;
    }
}
