package com.example.arvin.c3dispatch.RecyclerViewThings;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.arvin.c3dispatch.R;

/**
 * Created by Arvin on 27 Feb 2018.
 */

public class ChatTab extends Fragment{
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.chat_tab, container, false);

    }
}
