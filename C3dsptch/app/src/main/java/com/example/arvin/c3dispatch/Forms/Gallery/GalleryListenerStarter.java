package com.example.arvin.c3dispatch.Forms.Gallery;

import android.util.Log;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aaron on 23/04/2018.
 */

public class GalleryListenerStarter {

    public static Query sPhotosQuery;
    public static List<String> imgUrls;
    public static List<Boolean> selectedItems;


    public static void StartListening(String docID){

        imgUrls = new ArrayList<>();
        selectedItems = new ArrayList<>();

        sPhotosQuery = FirebaseFirestore.getInstance().collection("Events").document(docID)
                .collection("Conversation").whereEqualTo("type", "image");
        sPhotosQuery.get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot documentSnapshots) {
                for (DocumentSnapshot doc : documentSnapshots) {
                    imgUrls.add(doc.get("url").toString());

                    for (int x = 0; x < imgUrls.size(); x++) {
                        selectedItems.add(false);
                        Log.d("LoopSeletedItems", "added");
                    }


                }


            }
        });


    }

    public static List<String> getImgUrls(){
        return imgUrls;
    }

    public static List<Boolean> getSelectedItems(){
        return selectedItems;
    }






}
