package com.example.arvin.c3dispatch.Forms.Models;

/**
 * Created by Aaron on 13/02/2018.
 */

public class PatientAssessmentModel {

    private String date;
    private String location;
    private String patientFamilyName;
    private String patientGivenName;
    private String gender;
    private String DOB;
    private String patientAddress;
    private String telephone;
    private String allergies;
    private String medications;
    private String witnessFamilyName;
    private String witnessGivenName;
    private String pastMedicalHistory;


    public PatientAssessmentModel(String date, String location, String patientFamilyName, String patientGivenName, String gender, String DOB, String patientAddress, String telephone, String allergies, String medications, String witnessFamilyName, String witnessGivenName, String pastMedicalHistory) {
        this.date = date;
        this.location = location;
        this.patientFamilyName = patientFamilyName;
        this.patientGivenName = patientGivenName;
        this.gender = gender;
        this.DOB = DOB;
        this.patientAddress = patientAddress;
        this.telephone = telephone;
        this.allergies = allergies;
        this.medications = medications;
        this.witnessFamilyName = witnessFamilyName;
        this.witnessGivenName = witnessGivenName;
        this.pastMedicalHistory = pastMedicalHistory;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }


    public String getPatientFamilyName() {
        return patientFamilyName;
    }

    public void setPatientFamilyName(String patientFamilyName) {
        this.patientFamilyName = patientFamilyName;
    }

    public String getPatientGivenName() {
        return patientGivenName;
    }

    public void setPatientGivenName(String patientGivenName) {
        this.patientGivenName = patientGivenName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getPatientAddress() {
        return patientAddress;
    }

    public void setPatientAddress(String patientAddress) {
        this.patientAddress = patientAddress;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getAllergies() {
        return allergies;
    }

    public void setAllergies(String allergies) {
        this.allergies = allergies;
    }

    public String getMedications() {
        return medications;
    }

    public void setMedications(String medications) {
        this.medications = medications;
    }

    public String getWitnessFamilyName() {
        return witnessFamilyName;
    }

    public void setWitnessFamilyName(String witnessFamilyName) {
        this.witnessFamilyName = witnessFamilyName;
    }

    public String getWitnessGivenName() {
        return witnessGivenName;
    }

    public void setWitnessGivenName(String witnessGivenName) {
        this.witnessGivenName = witnessGivenName;
    }

    public String getPastMedicalHistory() {
        return pastMedicalHistory;
    }

    public void setPastMedicalHistory(String pastMedicalHistory) {
        this.pastMedicalHistory = pastMedicalHistory;
    }
}
