package com.example.arvin.c3dispatch.Forms;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.arvin.c3dispatch.Forms.Gallery.GalleryActivity;
import com.example.arvin.c3dispatch.Forms.Gallery.GalleryListenerStarter;
import com.example.arvin.c3dispatch.Forms.Models.NewPatientAssessmentModel;
import com.example.arvin.c3dispatch.Forms.Models.PatientAssessmentModel;
import com.example.arvin.c3dispatch.MyPushNotifications;
import com.example.arvin.c3dispatch.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

import me.riddhimanadib.formmaster.FormBuilder;
import me.riddhimanadib.formmaster.listener.OnFormElementValueChangedListener;
import me.riddhimanadib.formmaster.model.BaseFormElement;
import me.riddhimanadib.formmaster.model.FormElementPickerDate;
import me.riddhimanadib.formmaster.model.FormElementPickerMulti;
import me.riddhimanadib.formmaster.model.FormElementPickerSingle;
import me.riddhimanadib.formmaster.model.FormElementPickerTime;
import me.riddhimanadib.formmaster.model.FormElementTextMultiLine;
import me.riddhimanadib.formmaster.model.FormElementTextPhone;
import me.riddhimanadib.formmaster.model.FormElementTextSingleLine;
import me.riddhimanadib.formmaster.model.FormHeader;
import me.riddhimanadib.formmaster.viewholder.FormElementHeader;

public class NewPatientAssessment extends AppCompatActivity {

    private static final String TAG = "NewPatientAssessment";
    private RecyclerView mRecyclerView;
    private FormBuilder mFormBuilder;
    private List<BaseFormElement> formItems;
    private FirebaseFirestore db;
    private DialogInterface.OnClickListener dialogClickListener;
    private DialogInterface.OnClickListener dialogClickListenerForPhotos;
    private String eventId;
    private FirebaseAuth mAuth;
    private NewPatientAssessmentModel patient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_patient_assessment);
        db = FirebaseFirestore.getInstance();
        mAuth  = FirebaseAuth.getInstance();

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar_new_patient);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle((Html.fromHtml("<font color=\"#FFFFFF\">" + getString(R.string.patient_assessment) + "</font>")));

        patient = new NewPatientAssessmentModel();

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                eventId = null;
            } else {
                eventId = extras.getString("eventId");

            }

        }


        GalleryListenerStarter.StartListening(eventId);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView_new_patient);
        mFormBuilder = new FormBuilder(this, mRecyclerView);
        
        SetupForm();





        dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        //Yes button clicked


                        SubmitForm();
                        MyPushNotifications.NotifyReportSubmitted(getApplicationContext(), mAuth, eventId, "Patient Assessment");
                        finish();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };


    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == RequestCode.SELECT_PHOTOS){
            if(resultCode == RESULT_OK){
                // ilagay sa model yung photos
                 patient.setPhotos(data.getStringArrayListExtra("photos"));
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_with_send, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        if (id == R.id.action_send) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Are you sure you have filled out all the fields correctly?").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();
            // Do something
            return true;
        }else if (id == R.id.action_attach_photos){

            Intent intent = new Intent(getApplicationContext(), GalleryActivity.class);

            startActivityForResult(intent, RequestCode.SELECT_PHOTOS);
            return true;
        }


        return super.onOptionsItemSelected(item);
    }


    // babaguhin
    public void SubmitForm(){

        patient.setDate(formItems.get(1).getValue());
        patient.setLocation(formItems.get(2).getValue());
        patient.setTime(formItems.get(3).getValue());
        patient.setPatientFamilyName(formItems.get(4).getValue());
        patient.setPatientGivenName(formItems.get(5).getValue());
        patient.setPatientSex(formItems.get(6).getValue());
        patient.setPatientDOB(formItems.get(7).getValue());
        patient.setPatientAddress(formItems.get(8).getValue());
        patient.setPatientTelephone(formItems.get(9).getValue());
        patient.setAllergies(formItems.get(10).getValue());
        patient.setMedications(formItems.get(11).getValue());
        patient.setWhatHappened(formItems.get(12).getValue());
        patient.setWitnessFamilyName(formItems.get(13).getValue());
        patient.setWitnessGivenName(formItems.get(14).getValue());
        patient.setWitnessTelephone(formItems.get(15).getValue());
        patient.setPastMedicalHistory(formItems.get(16).getValue());
        patient.setTime1(formItems.get(19).getValue());
        patient.setBreathing1(formItems.get(20).getValue());
        patient.setPulse1(formItems.get(21).getValue());
        patient.setConscious1(formItems.get(22).getValue());
        patient.setOtherObservation1(formItems.get(23).getValue());
        patient.setTime2(formItems.get(25).getValue());
        patient.setBreathing2(formItems.get(26).getValue());
        patient.setPulse2(formItems.get(27).getValue());
        patient.setConscious2(formItems.get(28).getValue());
        patient.setOtherObservation2(formItems.get(29).getValue());
        patient.setTime3(formItems.get(31).getValue());
        patient.setBreathing3(formItems.get(32).getValue());
        patient.setPulse3(formItems.get(33).getValue());
        patient.setConscious3(formItems.get(34).getValue());
        patient.setOtherObservation3(formItems.get(35).getValue());
        patient.setRefustTreatment(formItems.get(37).getValue());
        patient.setDischarge(formItems.get(38).getValue());
        patient.setFirstAider(formItems.get(39).getValue());
        patient.setDoctorSignature(formItems.get(40).getValue());
        patient.setTimeOut(formItems.get(41).getValue());



        db.collection("Events")
                .document(eventId)
                .collection("Patient Assessments")
                .add(patient).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {

            @Override
            public void onSuccess(DocumentReference documentReference) {
                Log.d(TAG, "Successfully submitted");
            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(TAG, "Failed to Submit ;");
                        e.printStackTrace();
                    }
                });

        // insert code for sending notiications here

        // ipapasa sa gallery yung model
        // tas dun ilalagay yung photos
        finishActivity(0);
    }









    // babaguhin
    public void SetupForm() {
        // initialize variables
        List<String> listGender = new ArrayList<>();
        listGender.add("F");
        listGender.add("M");

        List<String> pastMedHistoryItems = new ArrayList<>();
        pastMedHistoryItems.add("Not known");
        pastMedHistoryItems.add("Asthma");
        pastMedHistoryItems.add("Cardiac");
        pastMedHistoryItems.add("Diabetic");
        pastMedHistoryItems.add("Epilepsy");
        pastMedHistoryItems.add("Hypertension");
        pastMedHistoryItems.add("Loss of Consciousness");
        pastMedHistoryItems.add("nil");
        pastMedHistoryItems.add("other?");

        List<String> listDischarge = new ArrayList<>();
        listDischarge.add("Ambulance");
        listDischarge.add("Hospital");
        listDischarge.add("Own Doctor");
        listDischarge.add("Return to Work");
        listDischarge.add("Other");















        FormHeader header1 = FormHeader.createInstance("Patient's Assessment Form");
        FormElementPickerDate date = FormElementPickerDate.createInstance().setTitle("Date").setDateFormat("MMM dd, yyyy").setHint("Date Today");
        FormElementTextSingleLine location = FormElementTextSingleLine.createInstance().setTitle("Location").setHint("Location");
        FormElementPickerTime time = FormElementPickerTime.createInstance().setTitle("Time").setHint("Time");
        FormElementTextSingleLine patientFamilyName = FormElementTextSingleLine.createInstance().setTitle("Patient's family name").setHint("Patient's family name");
        FormElementTextSingleLine patientGivenName = FormElementTextSingleLine.createInstance().setTitle("Given Name").setHint("Patient's given name");
        FormElementPickerSingle gender = FormElementPickerSingle.createInstance().setTitle("Sex").setHint("Sex");
        gender.setOptions(listGender);
        FormElementPickerDate DOB = FormElementPickerDate.createInstance().setTitle("DOB").setDateFormat("MMM dd, yyyy").setHint("Birthdate");
        FormElementTextSingleLine patientAddress = FormElementTextSingleLine.createInstance().setTitle("Patient's Address").setHint("Patient's address");
        FormElementTextPhone telephone = FormElementTextPhone.createInstance().setTitle("Telephone").setHint("Patient's telephone");
        FormElementTextMultiLine allergies = FormElementTextMultiLine.createInstance().setTitle("Allergies?").setHint("Allergies");
        FormElementTextMultiLine medications = FormElementTextMultiLine.createInstance().setTitle("Medications").setHint("Medications");
        FormElementTextSingleLine whatHappened = FormElementTextSingleLine.createInstance().setTitle("What happened?").setHint("What happened?");
        FormElementTextSingleLine witnessFamName = FormElementTextSingleLine.createInstance().setTitle("Witness' family name").setHint("Witness' family name");
        FormElementTextSingleLine witnessGivenName = FormElementTextSingleLine.createInstance().setTitle("Given Name").setHint("Witness' given name");
        FormElementTextSingleLine witnessTelephone = FormElementTextSingleLine.createInstance().setTitle("Telephone").setHint("Witness' telephone");


        FormElementPickerMulti pastMedHistory = FormElementPickerMulti.createInstance().setTitle("Past medical history").setHint("Past medical history");
        pastMedHistory.setOptions(pastMedHistoryItems);

        FormHeader headerObservation = FormHeader.createInstance("Observation");
        FormHeader observation1 = FormHeader.createInstance("Observation 1");
        FormElementPickerTime time1 = FormElementPickerTime.createInstance().setTitle("Time").setHint("Time");
        FormElementTextSingleLine breathing1 = FormElementTextSingleLine.createInstance().setTitle("Breathing").setHint("Breathing");
        FormElementTextSingleLine pulse1 = FormElementTextSingleLine.createInstance().setTitle("Pulse").setHint("Pulse");
        FormElementTextSingleLine conscious1 = FormElementTextSingleLine.createInstance().setTitle("Conscious").setHint("Conscious");
        FormElementTextSingleLine otherObservation1 = FormElementTextSingleLine.createInstance().setTitle("Other Observation").setHint("Other Observation");

        FormHeader observation2 = FormHeader.createInstance("Observation 2");
        FormElementPickerTime time2 = FormElementPickerTime.createInstance().setTitle("Time").setHint("Time");
        FormElementTextSingleLine breathing2 = FormElementTextSingleLine.createInstance().setTitle("Breathing").setHint("Breathing");
        FormElementTextSingleLine pulse2 = FormElementTextSingleLine.createInstance().setTitle("Pulse").setHint("Pulse");
        FormElementTextSingleLine conscious2 = FormElementTextSingleLine.createInstance().setTitle("Conscious").setHint("Conscious");
        FormElementTextSingleLine otherObservation2 = FormElementTextSingleLine.createInstance().setTitle("Other Observation");

        FormHeader observation3 = FormHeader.createInstance("Observation 3");
        FormElementPickerTime time3 = FormElementPickerTime.createInstance().setTitle("Time").setHint("Time");
        FormElementTextSingleLine breathing3 = FormElementTextSingleLine.createInstance().setTitle("Breathing").setHint("Breathing");
        FormElementTextSingleLine pulse3 = FormElementTextSingleLine.createInstance().setTitle("Pulse").setHint("Pulse");
        FormElementTextSingleLine conscious3 = FormElementTextSingleLine.createInstance().setTitle("Conscious").setHint("Conscious");
        FormElementTextSingleLine otherObservation3 = FormElementTextSingleLine.createInstance().setTitle("Other Observation");

        FormHeader header3 = FormHeader.createInstance("");
        FormElementTextSingleLine refuseTreatment = FormElementTextSingleLine.createInstance().setTitle("Refuse Treatment").setHint("witness name, number, and signature");
        FormElementPickerSingle discharge = FormElementPickerSingle.createInstance().setTitle("Discharge?").setHint("How?");
        discharge.setOptions(listDischarge);

        FormElementTextSingleLine firstAider = FormElementTextSingleLine.createInstance().setTitle("First Aider Name and signature").setHint("First aider name");
        FormElementTextSingleLine doctorSignature =FormElementTextSingleLine.createInstance().setTitle("Doctor's Signature").setHint("Doctor's name/signature");
        FormElementPickerTime timeOut = FormElementPickerTime.createInstance().setTitle("Time Out").setTimeFormat("KK:hh a").setHint("Time Out");




        formItems = new ArrayList<>();
        formItems.add(header1);// 0
        formItems.add(date); // 1
        formItems.add(location); // 2
        formItems.add(time); // 3
        formItems.add(patientFamilyName);
        formItems.add(patientGivenName);
        formItems.add(gender);
        formItems.add(DOB);
        formItems.add(patientAddress);
        formItems.add(telephone);
        formItems.add(allergies);
        formItems.add(medications);
        formItems.add(whatHappened);
        formItems.add(witnessFamName);
        formItems.add(witnessGivenName);
        formItems.add(witnessTelephone);
        formItems.add(pastMedHistory);
        formItems.add(headerObservation);// 17
        formItems.add(observation1); // 18
        formItems.add(time1);
        formItems.add(breathing1);
        formItems.add(pulse1);
        formItems.add(conscious1);
        formItems.add(otherObservation1);
        formItems.add(observation2); // 24
        formItems.add(time2);
        formItems.add(breathing2);
        formItems.add(pulse2);
        formItems.add(conscious2);
        formItems.add(otherObservation2);
        formItems.add(observation3);//30
        formItems.add(time3);
        formItems.add(breathing3);
        formItems.add(pulse3);
        formItems.add(conscious3);
        formItems.add(otherObservation3);
        formItems.add(header3); //36
        formItems.add(refuseTreatment);
        formItems.add(discharge);
        formItems.add(firstAider);
        formItems.add(doctorSignature);
        formItems.add(timeOut);
        mFormBuilder.addFormElements(formItems);


    }
}
