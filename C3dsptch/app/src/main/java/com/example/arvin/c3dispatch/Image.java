package com.example.arvin.c3dispatch;

/**
 * Created by Arvin on 17 Mar 2018.
 */

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by gbbarredo on 2/26/2018.
 */

public class Image {




    public Image(){

    }

    public String getPictureName(){

        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String formattedDate = df.format(new Date());
        return "C3" + formattedDate + ".jpg";

    }
}