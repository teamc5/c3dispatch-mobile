package com.example.arvin.c3dispatch.Forms;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;


import com.example.arvin.c3dispatch.Forms.Gallery.GalleryActivity;
import com.example.arvin.c3dispatch.Forms.Models.PatientAssessmentModel;
import com.example.arvin.c3dispatch.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

import me.riddhimanadib.formmaster.FormBuilder;
import me.riddhimanadib.formmaster.listener.OnFormElementValueChangedListener;
import me.riddhimanadib.formmaster.model.BaseFormElement;
import me.riddhimanadib.formmaster.model.FormElementPickerDate;
import me.riddhimanadib.formmaster.model.FormElementPickerMulti;
import me.riddhimanadib.formmaster.model.FormElementPickerSingle;
import me.riddhimanadib.formmaster.model.FormElementTextMultiLine;
import me.riddhimanadib.formmaster.model.FormElementTextPhone;
import me.riddhimanadib.formmaster.model.FormElementTextSingleLine;
import me.riddhimanadib.formmaster.model.FormHeader;


public class PatientAssessment extends AppCompatActivity {

    private static final String TAG = "PatientAssessment";
    private RecyclerView mRecyclerView;
    private FormBuilder mFormBuilder;
    private List<BaseFormElement> formItems;
    private FirebaseFirestore db;
    private DialogInterface.OnClickListener dialogClickListener;
    private String eventId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_assessment);
        db = FirebaseFirestore.getInstance();
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle((Html.fromHtml("<font color=\"#FFFFFF\">" + getString(R.string.patient_assessment) + "</font>")));


        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                eventId = null;
            } else {
                eventId = extras.getString("eventId");

            }

        }




        SetupForm();



        dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        //Yes button clicked
                        SubmitForm();
                        finish();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_with_send, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        if (id == R.id.action_send) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Are you sure you have filled out all the fields correctly?").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();
            // Do something
            return true;
        }else if (id == R.id.action_attach_photos){
            Intent intent = new Intent(getApplicationContext(), GalleryActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void SubmitForm(){
        PatientAssessmentModel patient = new PatientAssessmentModel(formItems.get(1).getValue(),
                formItems.get(2).getValue(),
                formItems.get(3).getValue(),
                formItems.get(4).getValue(),
                formItems.get(5).getValue(),
                formItems.get(6).getValue(),
                formItems.get(7).getValue(),
                formItems.get(8).getValue(),
                formItems.get(9).getValue(),
                formItems.get(10).getValue(),
                formItems.get(11).getValue(),
                formItems.get(12).getValue(),
                formItems.get(13).getValue());

        db.collection("Events")
                .document(eventId)
                .collection("Patient Assessments")
                .add(patient).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {

            @Override
            public void onSuccess(DocumentReference documentReference) {
                Log.d(TAG, "Successfully submitted");
            }
        })
        .addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(TAG, "Failed to Submit ;");
                e.printStackTrace();
            }
        });
                finishActivity(0);
    }






    public void SetupForm() {
        // initialize variables
        List<String> listGender = new ArrayList<>();
        listGender.add("Female");
        listGender.add("Male");

        List<String> pastMedHistoryItems = new ArrayList<>();
        pastMedHistoryItems.add("nil");
        pastMedHistoryItems.add("not known");
        pastMedHistoryItems.add("asthma");
        pastMedHistoryItems.add("hypertension");
        pastMedHistoryItems.add("cardiac");
        pastMedHistoryItems.add("diabetic");
        pastMedHistoryItems.add("loss of consciousness");


        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mFormBuilder = new FormBuilder(this, mRecyclerView, new OnFormElementValueChangedListener() {
            @Override
            public void onValueChanged(BaseFormElement formElement) {
                Toast.makeText(PatientAssessment.this, formElement.getValue(), Toast.LENGTH_SHORT).show();
            }
        });

        FormHeader header1 = FormHeader.createInstance("Patient's Assessment Form");
        FormElementPickerDate date = FormElementPickerDate.createInstance().setTitle("Date").setDateFormat("MMM dd, yyyy");
        FormElementTextSingleLine location = FormElementTextSingleLine.createInstance().setTitle("Location");
        //FormElementPickerTime time = FormElementPickerTime.createInstance().setTitle("Time").setTimeFormat("KK:hh a");
        FormElementTextSingleLine patientFamilyName = FormElementTextSingleLine.createInstance().setTitle("Patient's family name");
        FormElementTextSingleLine patientGivenName = FormElementTextSingleLine.createInstance().setTitle("Given Name");
        FormElementPickerSingle gender = FormElementPickerSingle.createInstance().setTitle("Gender");
        gender.setOptions(listGender);
        FormElementPickerDate DOB = FormElementPickerDate.createInstance().setTitle("DOB").setDateFormat("MMM dd, yyyy");
        FormElementTextSingleLine patientAddress = FormElementTextSingleLine.createInstance().setTitle("Patient's Address");
        FormElementTextPhone telephone = FormElementTextPhone.createInstance().setTitle("Telephone");
        FormElementTextMultiLine allergies = FormElementTextMultiLine.createInstance().setTitle("Allergies?");
        FormElementTextMultiLine medications = FormElementTextMultiLine.createInstance().setTitle("Medications");
        FormElementTextSingleLine witnessFamName = FormElementTextSingleLine.createInstance().setTitle("Witness' family name");
        FormElementTextSingleLine witnessGivenName = FormElementTextSingleLine.createInstance().setTitle("Given Name");
        FormElementPickerMulti pastMedHistory = FormElementPickerMulti.createInstance().setTitle("Past medical history");
        pastMedHistory.setOptions(pastMedHistoryItems);



        formItems = new ArrayList<>();
        formItems.add(header1);
        formItems.add(date);
        formItems.add(location);
        formItems.add(patientFamilyName);
        formItems.add(patientGivenName);
        formItems.add(gender);
        formItems.add(DOB);
        formItems.add(patientAddress);
        formItems.add(telephone);
        formItems.add(allergies);
        formItems.add(medications);
        formItems.add(witnessFamName);
        formItems.add(witnessGivenName);
        formItems.add(pastMedHistory);
        mFormBuilder.addFormElements(formItems);

     }
}

