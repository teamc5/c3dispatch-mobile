package com.example.arvin.c3dispatch.Chatting;

/**
 * Created by Arvin on 17 Mar 2018.
 */
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.example.arvin.c3dispatch.R;

import com.example.arvin.c3dispatch.RecyclerViewThings.Event;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Aaron on 10/03/2018.
 */

public class EventDetailsFragment extends Fragment {

    public final static String TAG = "EventDetailsFragment";
    private CollectionReference eventRef;
    private DocumentReference docRef;
    private Event event;


    @BindView(R.id.eventName) TextView eventName;

    @BindView(R.id.eventType) TextView eventType;

    @BindView(R.id.location)  TextView location;

    @BindView(R.id.timeOfCall) TextView timeOfCall;

    @BindView(R.id.status)  TextView status;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {



        View view = inflater.inflate(R.layout.fragment_event_details, container,false);

        ButterKnife.bind(this, view);

        event = ((FirestoreChatActivity)getActivity()).getEvent();
        eventName.setText("Event: "+event.getEventName());
        eventType.setText("Event Type: "+event.getEventType());
        location.setText("Location: "+ event.getLocation());
        timeOfCall.setText("Time of Call: "+ event.getTimeOfCall());
        status.setText("Status: "+ event.getStatus());

        //Toast.makeText(getActivity(), event.getTimeOfCall() + "", Toast.LENGTH_SHORT).show();
        // queue here
/*        eventRef = FirebaseFirestore.getInstance().collection("Events");
        docRef = eventRef.document(((FirestoreChatActivity)getActivity()).getCollectionName());
        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
               event =  documentSnapshot.toObject(Event.class);
                //Toast.makeText(getActivity(), event.getEventName() + " Listener", Toast.LENGTH_SHORT).show();
                eventName.setText("Event: "+event.getEventName());
                eventType.setText("Event Type: "+event.getEventType());
                location.setText("Location: "+ event.getLocation());
                timeOfCall.setText("Time of Call: "+ event.getTimeOfCall());
                status.setText("Status: "+ event.getStatus());
            }
        });*/



        // Toast.makeText(getActivity(), "Fragment Added!", Toast.LENGTH_SHORT).show();

        // Toast.makeText(getActivity(),((FirestoreChatActivity)getActivity()).getCollectionName(), Toast.LENGTH_SHORT).show();

        return view;
    }




}
