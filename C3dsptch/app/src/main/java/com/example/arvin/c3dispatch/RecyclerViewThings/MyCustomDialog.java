package com.example.arvin.c3dispatch.RecyclerViewThings;

import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.arvin.c3dispatch.MyPushNotifications;
import com.example.arvin.c3dispatch.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

/**
 * Created by Status on 12/10/2017.
 */

public class MyCustomDialog extends DialogFragment {

    private static final String TAG = "MyCustomDialog";

    public interface OnInputListener{
        void sendInput(String input);
    }

    //widgets
    private EditText mInput;
    private TextView mMessage;
    private Button mRespond;
    private MyPushNotifications myPushNotifications;
    private FirebaseAuth mAuth;
    private String docId;
    private String message;


    public MyCustomDialog(){

    }


    //vars

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_event_notification, container, false);

        mMessage = view.findViewById(R.id.dialog_event_header);

        mRespond = view.findViewById(R.id.respond_button);
        mAuth = FirebaseAuth.getInstance();
        myPushNotifications = new MyPushNotifications();
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            docId = bundle.getString("docId");
            message = bundle.getString("message");
            mMessage.setText(message);
            myPushNotifications.GetEvent(docId);
        }



/*        mActionCancel = view.findViewById(R.id.action_cancel);
        mActionOk = view.findViewById(R.id.action_ok);
        mInput = view.findViewById(R.id.input);

        mActionCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: closing dialog");
                Toast.makeText(getActivity(), "You did not respond", Toast.LENGTH_SHORT).show();
                getDialog().dismiss();
            }
        });*/


        mRespond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: capturing input");

            //    String input = mInput.getText().toString();
//                if(!input.equals("")){
//
//                    //Easiest way: just set the value
//                    ((MainActivity)getActivity()).mInputDisplay.setText(input);
//
//                }

                //"Best Practice" but it takes longer
                //mOnInputListener.sendInput(input);
                // send notification here na nag respond na yung responder

                myPushNotifications.SendRespondNotificationToServer(getActivity().getApplicationContext(), mAuth, docId);
                //Toast.makeText(getActivity(), "You have responded", Toast.LENGTH_SHORT).show();



                getDialog().dismiss();
            }
        });

        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        }

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{

        }catch (ClassCastException e){
            Log.e(TAG, "onAttach: ClassCastException: " + e.getMessage() );
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if(getActivity()!= null)
            getActivity().finish();
        super.onDismiss(dialog);

    }
}


















