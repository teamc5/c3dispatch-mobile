package com.example.arvin.c3dispatch.RecyclerViewThings;

/**
 * Created by Arvin on 17 Mar 2018.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.firestore.GeoPoint;

/**
 * Created by Aaron on 10/03/2018.
 */

public class GeoNew implements Parcelable {

    private String something;
    private double latitude, longitude;
    private GeoPoint geoPoint;

    public GeoNew() {
    }

    protected GeoNew(Parcel in) {
        something = in.readString();
        latitude = in.readDouble();
        longitude = in.readDouble();
        geoPoint = new GeoPoint(latitude, longitude);
    }

    public static final Creator<GeoNew> CREATOR = new Creator<GeoNew>() {
        @Override
        public GeoNew createFromParcel(Parcel in) {
            return new GeoNew(in);
        }

        @Override
        public GeoNew[] newArray(int size) {
            return new GeoNew[size];
        }
    };

    public String getSomething() {
        return something;
    }

    public void setSomething(String something) {
        this.something = something;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public GeoPoint getGeoPoint() {
        return geoPoint;
    }

    public void setGeoPoint(GeoPoint geoPoint) {
        this.geoPoint = geoPoint;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        latitude = getGeoPoint().getLatitude();
        longitude = getGeoPoint().getLongitude();
        parcel.writeString(something);
        parcel.writeDouble(latitude);
        parcel.writeDouble(longitude);
    }
}