package com.example.arvin.c3dispatch.Chatting;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.example.arvin.c3dispatch.BuildConfig;
import com.example.arvin.c3dispatch.Chat;
import com.example.arvin.c3dispatch.Chatting.InstantMessage.InstantMessage;
import com.example.arvin.c3dispatch.Chatting.InstantMessage.InstantMessageRecyclerAdapter;
import com.example.arvin.c3dispatch.Chatting.InstantMessage.KeyboardHeightObserver;
import com.example.arvin.c3dispatch.Chatting.InstantMessage.KeyboardHeightProvider;
import com.example.arvin.c3dispatch.Forms.FormsMainActivity;
import com.example.arvin.c3dispatch.Forms.NewForms.TestRecycler;
import com.example.arvin.c3dispatch.Image;
import com.example.arvin.c3dispatch.Photo;
import com.example.arvin.c3dispatch.R;
import com.example.arvin.c3dispatch.RecyclerViewThings.Event;
import com.example.arvin.c3dispatch.RecyclerViewThings.GeoNew;
import com.example.arvin.c3dispatch.RecyclerViewThings.MyDialog;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.firestore.Query;
import com.google.firebase.storage.UploadTask;
import com.pusher.pushnotifications.PushNotifications;
import com.squareup.picasso.Picasso;

import android.view.ViewGroup.LayoutParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.microedition.khronos.opengles.GL;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
/**
 * Created by Aaron on 21/02/2018.
 */

public class FirestoreChatActivity extends AppCompatActivity implements KeyboardHeightObserver, InstantMessageRecyclerAdapter.OnItemClicked {

    private static final String TAG = "FirestoreChatActivity";
    private static String docId;
    private static String collectionName;
    private CollectionReference sChatCollection;
    private CollectionReference sPhotoCollection;
    private Query sChatQuery;
    private Query sChatSubCollectionQuery;
    private File imageFile;
    private Uri imgUri;
    private StorageReference mStorage;
    private Event eventThis;
    private EventDetailsFragment fragment;
    private FirebaseAuth mAuth;
    private String mFileName = null;
    private ProgressDialog mProgress;
    private MediaRecorder mRecorder;
    private ImageButton mRecordBtn;
    private MediaPlayer mediaPlayer;
    Image i;


    // for instant message popup
    private View popUpView;
    private List<InstantMessage> instantMessageList;
    private PopupWindow popupWindow;
    private boolean isKeyBoardVisible;
    private int keyboardHeight;
    @BindView(R.id.im_button)
    ImageView instantViewBtn;
    @BindView(R.id.firestorechat_parent_layout)
    RelativeLayout parentLayout;
    @BindView(R.id.footer_for_instant_message)
    LinearLayout instantMessageCover;

    private RecyclerView instantMessageRecyclerView;

    private InstantMessageRecyclerAdapter instantMessageRecyclerAdapter;
    private KeyboardHeightProvider keyboardHeightProvider;
    //end instant message popup

    @BindView(R.id.messagesList)
    RecyclerView mRecyclerView;

    @BindView(R.id.sendButton)
    Button mSendButton;

    @BindView(R.id.messageEdit)
    EditText mMessageEdit;

    @BindView(R.id.emptyTextView)
    TextView mEmptyListMessage;

    @BindView(R.id.floatingActionButton3)
    ImageButton mFab;

    //    @BindView(R.id.imageView)
    ImageView mImgView;

    @BindView(R.id.chat_toolbar)
    Toolbar chat_toolbar;

    @BindView(R.id.footer)
    LinearLayout footer;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firestore_chat);
        ButterKnife.bind(this);


        mediaPlayer = new MediaPlayer();
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mProgress = new ProgressDialog(this);
        mRecordBtn = findViewById(R.id.floatingMic);
        mRecordBtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    startRecording();
                } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    stopRecording();
                }
                return false;
            }
        });


        // instant message
//        final float popUpheight = getResources().getDimension(
//                R.dimen.keyboard_height);
//        changeKeyboardHeight((int) popUpheight);
        checkKeyboardHeight(parentLayout);

        keyboardHeightProvider = new KeyboardHeightProvider(this);

        // make sure to start the keyboard height provider after the onResume
        // of this activity. This is because a popup window must be initialised
        // and attached to the activity root view.
        View view = findViewById(R.id.firestorechat_parent_layout);
        view.post(new Runnable() {
            public void run() {
                keyboardHeightProvider.start();
            }
        });




        instantViewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                if (!popupWindow.isShowing()) {

                    popupWindow.setHeight((int) (keyboardHeight));


                    if (isKeyBoardVisible) {
                        instantMessageCover.setVisibility(LinearLayout.GONE);
//                                getWindow().setSoftInputMode(WindowManager.LayoutParams.
//                                        SOFT_INPUT_ADJUST_RESIZE);
//                                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
                    } else {

                                InputMethodManager inputMethodManager =
                                        (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                                inputMethodManager.toggleSoftInputFromWindow(
                                        parentLayout.getApplicationWindowToken(),
                                        InputMethodManager.SHOW_FORCED, 0);
                        //getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
//                        InputMethodManager imm = (InputMethodManager)   getSystemService(Context.INPUT_METHOD_SERVICE);
//                        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                        //imm.showSoftInput(instantViewBtn, InputMethodManager.SHOW_IMPLICIT);
                        checkKeyboardHeight(parentLayout);
                        instantMessageCover.setVisibility(LinearLayout.VISIBLE);


                    }
                    popupWindow.showAtLocation(parentLayout, Gravity.BOTTOM, 0, 0);

                } else {
                    popupWindow.dismiss();
                }

            }
        });



        popUpView = getLayoutInflater().inflate(R.layout.popup_instant_message_view, null);
        popupWindow = new PopupWindow(popUpView, FrameLayout.LayoutParams.MATCH_PARENT, (int) keyboardHeight, false);

        instantMessageRecyclerView = popUpView.findViewById(R.id.im_recycler);

        instantMessageList = new ArrayList<>();
        instantMessageRecyclerAdapter = new InstantMessageRecyclerAdapter(instantMessageList);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        instantMessageRecyclerView.setLayoutManager(mLayoutManager);
        instantMessageRecyclerView.setItemAnimator(new DefaultItemAnimator());


        PopulateInstantMessage();


        instantMessageRecyclerView.setAdapter(instantMessageRecyclerAdapter);
        instantMessageRecyclerAdapter.notifyDataSetChanged();

        instantMessageRecyclerAdapter.setOnClick(this);








        //checkKeyboardHeight(parentLayout);
        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {

            @Override
            public void onDismiss() {
                instantMessageCover.setVisibility(LinearLayout.GONE);
            }
        });
        mMessageEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow.isShowing()) {

                    popupWindow.dismiss();

                }
            }
        });




        // end instant message


        mFileName = Environment.getExternalStorageDirectory().getAbsolutePath();
        mFileName += "/" + getFileName();

        // Setting ActionBar
        setSupportActionBar(chat_toolbar);
        fragment = new EventDetailsFragment();

        mAuth = FirebaseAuth.getInstance();
        mStorage = FirebaseStorage.getInstance().getReference();
        i = new Image();

        mImgView = findViewById(R.id.imageView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        //NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);


        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                collectionName = null;
            } else {
                collectionName = extras.getString("collectionName");
                // eventThis = (Event) extras.getSerializable("EventObject");
                GeoNew mapLocation = getIntent().getParcelableExtra("mapLocation");

                Date dateObj = new Date(extras.getLong("date", -1));
                eventThis = getIntent().getParcelableExtra("eventObject");
                eventThis.setMapLocation(mapLocation.getGeoPoint());
                eventThis.setTimeOfCall(dateObj);

                //Toast.makeText(this, eventThis.getMapLocation()+" GEO Waow", Toast.LENGTH_SHORT).show();
                //Toast.makeText(this, eventThis.getTimeOfCall()+"", Toast.LENGTH_SHORT).show();

                // set this event sa event details

                getSupportActionBar().setTitle(Html.fromHtml("<font color=\"#ffffff\">" + extras.getString("eventName") + "</font>"));

                //Toast.makeText(this, ""+ PushNotifications.getSubscriptions(), Toast.LENGTH_LONG).show();
                Log.d(TAG, "Pusher subscriptions: " + PushNotifications.getSubscriptions());
            }


        }

        // Query for conversation collection
        sChatCollection = FirebaseFirestore.getInstance().collection("Events")
                .document(collectionName).collection("Conversation");

        sChatQuery = sChatCollection.orderBy("timestamp").limit(200);


        sPhotoCollection = FirebaseFirestore.getInstance().collection("Events")
                .document(collectionName).collection("Photos");

        attachRecyclerViewAdapter();


        if(eventThis.getStatus().equals("Finished")){
//            mMessageEdit.setEnabled(false);
//            mSendButton.setEnabled(false);
//            mFab.setEnabled(false);
//            mRecordBtn.setEnabled(false);
            footer.setVisibility(View.GONE);


        }
    }

    // InstantMessage Click event
    @Override
    public void onItemClick(int position) {

        // The onClick implementation of the RecyclerView item click
        //ur intent code here

        String uid = mAuth.getUid();
        String name = mAuth.getUid();
        //GLOBALS.isMessage = true;
        onAddMessage(new Chat(name, instantMessageRecyclerAdapter.getItem(position).getText(), uid, "text"));
    }

    private void PopulateInstantMessage(){
        InstantMessage im = new InstantMessage("En route to location!");
        instantMessageList.add(im);

        im = new InstantMessage("Arrived at location!");
        instantMessageList.add(im);

        im = new InstantMessage("Incident ended!");
        instantMessageList.add(im);
    }


    // added April 12
    int previousHeightDiffrence = 0;
    private void checkKeyboardHeight(final View parentLayout) {

        parentLayout.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {

                    @Override
                    public void onGlobalLayout() {

                        Rect r = new Rect();
                        parentLayout.getWindowVisibleDisplayFrame(r);

                        int screenHeight = parentLayout.getRootView()
                                .getHeight();
                        int heightDifference = screenHeight - (r.bottom);

                        if (previousHeightDiffrence - heightDifference > 50) {
                            popupWindow.dismiss();
                        }

                        previousHeightDiffrence = heightDifference;
                        if (heightDifference > 100) {

                            isKeyBoardVisible = true;
                            changeKeyboardHeight(heightDifference);

                        } else {

                            isKeyBoardVisible = false;

                        }

                    }
                });

    }

    private void changeKeyboardHeight(int height) {

        if (height > 100) {
            keyboardHeight = height;
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, keyboardHeight);
            instantMessageCover.setLayoutParams(params);
        }

    }

    @Override
    public void onKeyboardHeightChanged(int height, int orientation) {

        String or = orientation == Configuration.ORIENTATION_PORTRAIT ? "portrait" : "landscape";
        Log.i(TAG, "onKeyboardHeightChanged in pixels: " + height + " " + or);



//        // color the keyboard height view, this will stay when you close the keyboard
        View view = findViewById(R.id.footer_for_instant_message);
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams)view.getLayoutParams();
        params.height = height;
        //keyboardHeight = height;
        //changeKeyboardHeight(keyboardHeight);
        instantMessageCover.setLayoutParams(params);
        Log.d("KeyboardHeight", keyboardHeight+"");
    }

    @Override
    public void onPause() {
        super.onPause();
        keyboardHeightProvider.setKeyboardHeightObserver(null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onResume() {
        super.onResume();
        keyboardHeightProvider.setKeyboardHeightObserver(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        keyboardHeightProvider.close();
    }

// end April 12


    // start ActionBar Items
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId()) {
            case R.id.action_event_details:
                // Status chose the "Settings" item, show the app settings UI...
                // show event details fragment
                //setViewPager(0);
                String backStateName = EventDetailsFragment.class.getName();
                String fragmentTag = backStateName;
                FragmentManager manager = getSupportFragmentManager();
                boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);
                // if(!fragmentPopped) {

                FragmentTransaction transaction = getSupportFragmentManager()
                        .beginTransaction();

                //transaction.replace(R.id.container_layout, fragment);
                if (!fragment.isAdded()) {
                    //if (fragmentPopped) {
                    transaction.addToBackStack(backStateName).add(R.id.container_layout, fragment).commit();
                    //} else {

                    // }
                    //return true; //or return false/true, based on where you are calling from
                } else
                    transaction.replace(R.id.container_layout, fragment).commit();


                       /* transaction.replace(R.id.container_layout, fragment, fragmentTag);

                        transaction.addToBackStack(backStateName).commit();
*/
              /*  int fragments = getSupportFragmentManager().getBackStackEntryCount();
                Toast.makeText(this, fragments+"", Toast.LENGTH_SHORT).show();*/
                return true;
            //}
            case R.id.action_submit_form:

                Intent intent = new Intent(getApplicationContext(), FormsMainActivity.class);
//                Intent intent = new Intent(getApplicationContext(), TestRecycler.class);
                intent.putExtra("eventId", eventThis.getEventId());

//                Date dateObj = eventThis.getTimeOfCall();
//
//                intent.putExtra("date", dateObj);
                intent.putExtra("date", eventThis.getTimeOfCall().getTime());

                startActivity(intent);


                return true;


            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    // end ActionBar Items


    // OnClick for sendButton
    @OnClick(R.id.sendButton)
    public void onSendClick() {

        // should be replaced with FirebaseUID
        String uid = mAuth.getUid();
        String name = mAuth.getUid();
        //GLOBALS.isMessage = true;
        onAddMessage(new Chat(name, mMessageEdit.getText().toString(), uid, "text"));
        mMessageEdit.setText("");
    }

    private void attachRecyclerViewAdapter() {
        final RecyclerView.Adapter adapter = newAdapter();

        // Scroll to bottom on new messages
        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                mRecyclerView.smoothScrollToPosition(adapter.getItemCount());
            }
        });

        mRecyclerView.setAdapter(adapter);
    }


    protected RecyclerView.Adapter newAdapter() {
        FirestoreRecyclerOptions<Chat> options =
                new FirestoreRecyclerOptions.Builder<Chat>()
                        .setQuery(sChatQuery, Chat.class)
                        .setLifecycleOwner(this)
                        .build();

        return new FirestoreRecyclerAdapter<Chat, ChatHolder>(options) {
            @Override
            public ChatHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                //  if(GLOBALS.isMessage) {
                return new ChatHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.message, parent, false));
//                }
//                else{
//                    return new ChatHolder(LayoutInflater.from(parent.getContext())
//                            .inflate(R.layout.image, parent, false));
//                }
            }

            @Override
            protected void onBindViewHolder(@NonNull ChatHolder holder, int position, @NonNull Chat model) {
                holder.bind(model);
                final Chat c = model;

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (c.getType().equals("image")) {
                            //Toast.makeText(getApplicationContext(), "asd", Toast.LENGTH_SHORT).show();
//                            Intent intentEvent = new Intent(getApplicationContext(), MyDialog.class);
//                            intentEvent.putExtra("Type", "image");
//                            intentEvent.putExtra("url", c.getUrl());
//                            startActivity(intentEvent);

                        }
                        else if (c.getType().equals("voice") || c.getType().equals("audio")) {
//                            try {
//                                mediaPlayer.setDataSource(c.getUrl());
//                                mediaPlayer.prepareAsync();
//                                mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//                                    @Override
//                                    public void onPrepared(MediaPlayer mediaPlayer) {
//                                        mediaPlayer.start();
//                                    }
//                                });
//                                if(mediaPlayer.isPlaying()){
//                                    mediaPlayer.pause();
//
//                                }
//                                else{
//                                    mediaPlayer.start();
//                                }
//                            } catch (IOException e) {
//                                e.printStackTrace();
//                            }
                            //Toast.makeText(getApplicationContext(), "asd", Toast.LENGTH_SHORT).show();
                            Uri temp = Uri.parse(c.getUrl());
                            Intent viewMediaIntent = new Intent();
                            viewMediaIntent.setAction(android.content.Intent.ACTION_VIEW);
                            viewMediaIntent.setDataAndType(temp, "audio/*");
                            viewMediaIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(viewMediaIntent);

                        }
                    }
                });

            }

            @Override
            public void onDataChanged() {
                // If there are no chat messages, show a view that invites the user to add a message.
                mEmptyListMessage.setVisibility(getItemCount() == 0 ? View.VISIBLE : View.GONE);
            }
        };
    }


    protected void onAddMessage(Chat chat) {
        final String message = chat.getMessage();
        sChatCollection.add(chat).addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e(TAG, "Failed to write message", e);
            }
        })
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        //docId = documentReference.getId().toString();
                        //Toast.makeText(getApplicationContext(), docId + " " + collectionName, Toast.LENGTH_SHORT).show();

                        // lagay dito yung pag send ng notif sa api
                        SendPushNotificationToServer(message);

                    }
                });


    }
    protected void addPhoto(Photo photo) {
        //final String message = chat.getMessage();
        sPhotoCollection.add(photo).addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e(TAG, "Failed to write message", e);
            }
        })
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        //docId = documentReference.getId().toString();
                        //Toast.makeText(getApplicationContext(), docId + " " + collectionName, Toast.LENGTH_SHORT).show();

                        // lagay dito yung pag send ng notif sa api
                        //SendPushNotificationToServer(message);

                    }
                });


    }


    private void SendPushNotificationToServer(String chat) {
        try {
            String url = "http://c3dispatch-env.us-east-2.elasticbeanstalk.com/api/sendpush";
            //String url = "http://192.168.0.105:80/dispatch2/public/api/sendpush";
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("title", eventThis.getEventName());
            jsonBody.put("message", mAuth.getUid() + ": " + chat);
            //jsonBody.put("type", "message");
            jsonBody.put("type", "message");
            jsonBody.put("sender", mAuth.getUid());
            jsonBody.put("channel_name", eventThis.getEventId());
            //jsonBody.put("channel_name", "cmd_center");
            final String requestBody = jsonBody.toString();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {


                    //Toast.makeText(getApplicationContext(), "notif not sent", Toast.LENGTH_LONG).show();
                    Log.i("VOLLEY", response);
                   // Toast.makeText(FirestoreChatActivity.this, response, Toast.LENGTH_SHORT).show();
                    //

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
//
                    Toast.makeText(FirestoreChatActivity.this, "failed to send", Toast.LENGTH_SHORT).show();
                }
            }) {


                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

               /* @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json");
                    return params;
                }*/
            };

            requestQueue.add(stringRequest);


        } catch (JSONException e) {

        }


        try {
            String url = "http://c3dispatch-env.us-east-2.elasticbeanstalk.com/api/sendnotif";
            //String url = "http://192.168.0.105:80/dispatch2/public/api/sendpush";
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("title", eventThis.getEventName());
            jsonBody.put("message", mAuth.getUid() + ": " + chat);
            //jsonBody.put("type", "message");
            jsonBody.put("type", "message");
            jsonBody.put("sender", mAuth.getUid());
            jsonBody.put("channel_name", eventThis.getEventId());
            //jsonBody.put("channel_name", "cmd_center");
            final String requestBody = jsonBody.toString();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {


                    //Toast.makeText(getApplicationContext(), "notif not sent", Toast.LENGTH_LONG).show();
                    Log.i("VOLLEY", response);
                    // Toast.makeText(FirestoreChatActivity.this, response, Toast.LENGTH_SHORT).show();
                    //

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
//
                    Toast.makeText(FirestoreChatActivity.this, "failed to send", Toast.LENGTH_SHORT).show();
                }
            }) {


                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

               /* @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json");
                    return params;
                }*/
            };

            requestQueue.add(stringRequest);


        } catch (JSONException e) {

        }
    }

    @OnClick(R.id.floatingActionButton3)
    public void onCameraClick() {

        //GLOBALS.isMessage = false;
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        String file_path = Environment.getExternalStoragePublicDirectory("C3").toString();

        File dir = new File(file_path);
        if(!dir.exists())
            Toast.makeText(getApplicationContext(), ""+dir.mkdirs(), Toast.LENGTH_LONG).show();


        imageFile = new File(Environment.getExternalStoragePublicDirectory("C3"), i.getPictureName());
        imgUri = FileProvider.getUriForFile(FirestoreChatActivity.this, BuildConfig.APPLICATION_ID + ".provider",imageFile);


         //Toast.makeText(getApplicationContext(), imgUri.toString(), Toast.LENGTH_SHORT).show();
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imgUri);
        startActivityForResult(cameraIntent, 0);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0) {
            StorageReference filePath = mStorage.child("Photos").child(i.getPictureName());
            final String uid = mAuth.getUid();
            final String name = mAuth.getUid();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imgUri);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 70 , byteArrayOutputStream);
                String path = MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, "Image", null);
                imgUri = Uri.parse(path);
                filePath.putFile(imgUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        //GLOBALS.url = taskSnapshot.getDownloadUrl().toString();
                        addPhoto(new Photo(taskSnapshot.getDownloadUrl().toString()));
                        onAddMessage(new Chat(name, "has sent an image", uid, taskSnapshot.getDownloadUrl().toString(), "image"));

                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }



        }


    }


    public Event getEvent() {
        return eventThis;
    }

 /*   private void setupViewPager(ViewPager viewPager){
        SectionsStatePagerAdapter adapter = new SectionsStatePagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new EventDetailsFragment(), "EventDetailsFragment");

        viewPager.setAdapter(adapter);

    }*/

   /* public void setViewPager(int fragmentNumber){
        mViewPager.setCurrentItem(fragmentNumber);
    }*/

    @Override
    public void onBackPressed() {


        int fragments = getSupportFragmentManager().getBackStackEntryCount();
        //Toast.makeText(this, fragments+"", Toast.LENGTH_SHORT).show();
        if (fragments == 0) {
            finish();
        } else {
            if (getFragmentManager().getBackStackEntryCount() > 0) {
                getFragmentManager().popBackStack();
            } else {
                super.onBackPressed();
            }
        }
    }

    private void startRecording() {
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.AAC_ADTS);
        mRecorder.setOutputFile(mFileName);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        try {
            mRecorder.prepare();

            mRecorder.start();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    private void stopRecording() {

        mRecorder.stop();
        mRecorder.release();
        mRecorder = null;

        uploadAudio();

    }

    private void uploadAudio() {

        mProgress.setMessage("Uploading Audio...");
        mProgress.show();
        final String uid = mAuth.getUid();
        final String name = mAuth.getUid();

        StorageReference filePath = mStorage.child("Audio").child(getFileName());
        Uri uri = Uri.fromFile(new File(mFileName));
        filePath.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                onAddMessage(new Chat(name, getFileName(), uid, taskSnapshot.getDownloadUrl().toString(), "voice"));
                mProgress.dismiss();
            }
        });

    }
    private String getFileName(){

        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String formattedDate = df.format(new Date());
        return "C3" + formattedDate + ".aac";

    }
}
