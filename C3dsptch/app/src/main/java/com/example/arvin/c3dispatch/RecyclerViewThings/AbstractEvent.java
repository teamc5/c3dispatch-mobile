package com.example.arvin.c3dispatch.RecyclerViewThings;

import android.os.Parcelable;

import com.google.firebase.firestore.GeoPoint;

import java.util.Date;

/**
 * Created by Aaron on 23/02/2018.
 */

public abstract class AbstractEvent implements Parcelable {


    public abstract String getEventId();

    public abstract String getEventName();

    public abstract String getEventType();


    public abstract String getLocation();

    public abstract GeoPoint getMapLocation();

    public abstract String getStatus();

    public abstract Date getTimeOfCall();
}