package com.example.arvin.c3dispatch.Chatting;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;


import com.example.arvin.c3dispatch.R;
import com.github.bassaer.chatmessageview.model.ChatUser;
import com.github.bassaer.chatmessageview.model.Message;
import com.github.bassaer.chatmessageview.view.ChatView;

public class ChatActivity extends AppCompatActivity {

    private ChatView chatView;



    private static final String TAG = "FirestoreChatActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        chatView = (ChatView) findViewById(R.id.chat_view);
        //Status id
        int myId = 0;
//Status icon
        Bitmap myIcon = BitmapFactory.decodeResource(getResources(), R.drawable.face_2);
//Status name
        String myName = "Michael";

        int yourId = 1;
        Bitmap yourIcon = BitmapFactory.decodeResource(getResources(), R.drawable.face_1);
        String yourName = "Emily";

        final ChatUser me = new ChatUser(myId, myName, myIcon);
        final ChatUser you = new ChatUser(yourId, yourName, yourIcon);



        Message message1 = new Message.Builder()
                .setUser(me) // Sender
                .setRightMessage(true) // This message Will be shown right side.
                .setMessageText("Hello!") //Message contentt
                .build();
        Message message2 = new Message.Builder()
                .setUser(you) // Sender
                .setRightMessage(false) // This message Will be shown left side.
                .setMessageText("What's up?") //Message contents
                .build();

        chatView.send(message1); // Will be shown right side
        chatView.receive(message2); // Will be shown left side




        // start of Firestore implementation


    }
}
