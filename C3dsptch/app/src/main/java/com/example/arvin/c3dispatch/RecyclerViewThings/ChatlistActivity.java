package com.example.arvin.c3dispatch.RecyclerViewThings;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.arvin.c3dispatch.Chatting.FirestoreChatActivity;
import com.example.arvin.c3dispatch.MainActivity;
import com.example.arvin.c3dispatch.MainMenu;
import com.example.arvin.c3dispatch.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.messaging.RemoteMessage;
import com.pusher.pushnotifications.PushNotificationReceivedListener;
import com.pusher.pushnotifications.PushNotifications;
import com.pusher.pushnotifications.api.PushNotificationService;
import com.pusher.pushnotifications.api.PushNotificationsAPI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ChatlistActivity extends Fragment {


    private static final String TAG = "ChatlistActivity";
    private CollectionReference sChatlistCollection= FirebaseFirestore.getInstance().collection("Events");
    private Query sChatQuery = sChatlistCollection.whereEqualTo("status","Ongoing").orderBy("timeOfCall", Query.Direction.DESCENDING).limit(200);
    protected RecyclerView.LayoutManager mLayoutManager;
    private FirebaseAuth mAuth;
    private List<Event> listEvent;


    private List<Event> allEventsList;
    private List<Event> ongoingEvents;
    private HashMap<String, Event> ongoingEventsMap;


    private Context chatlistContext;


    public ChatlistActivity() {

    }

    @BindView(R.id.chat_list)
    RecyclerView mRecyclerView;

    @BindView(R.id.my_toolbar)
    Toolbar myToolbar;

    @BindView(R.id.spinner_status)
    Spinner spinner;

    @BindView(R.id.test_button)
    Button button;

    @Override
    public void onAttach(Context context) {
        chatlistContext = context;
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        mAuth = FirebaseAuth.getInstance();
        allEventsList = new ArrayList<>();
        //ongoingEvents = new ArrayList<>();
        View rootView = inflater.inflate(R.layout.activity_chatlist, container, false);

        ButterKnife.bind(this, rootView);
        ((AppCompatActivity)getActivity()).setSupportActionBar(myToolbar);

        (getActivity()).setTitle(Html.fromHtml("<font color=\"#ffffff\">" +"" + "</font>"));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        sChatlistCollection = FirebaseFirestore.getInstance().collection("Events");


        // spinner

        // add a custom text view para ma edit yung text sa laman nung spinnner
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.spinner_status_array, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.getBackground().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                Log.d("Spinner",adapterView.getItemAtPosition(i).toString());
                if(adapterView.getChildAt(0)!= null){
                    ((TextView)adapterView.getChildAt(0)).setTextColor(Color.WHITE);
                    ((TextView)adapterView.getChildAt(0)).setTextSize(getResources().getDimension(R.dimen.spinner_text));
                }

                if(adapterView.getItemAtPosition(i).toString().equals("Ongoing Events")){
                    Log.d("Spinner", "pumasok sa unang if");
                    //clearRecyclerViewData();
                    ongoingEvents = new ArrayList<>();
                    ongoingEventsMap = new HashMap<>();
                    sChatQuery = sChatlistCollection.whereEqualTo("status","Ongoing").orderBy("timeOfCall", Query.Direction.DESCENDING).limit(200);
                    attachRecyclerViewAdapter();

                    Log.d("ListSpinner", ""+listEvent.size());

                }else if (adapterView.getItemAtPosition(i).toString().equals("Finished Events")){
                    Log.d("Spinner", "pumasok sa pangalawang if");
                    //clearRecyclerViewData();
                    sChatQuery = sChatlistCollection.whereEqualTo("status","Finished").orderBy("timeOfCall", Query.Direction.DESCENDING).limit(200);
                    attachRecyclerViewAdapter();
                    Log.d("ListSpinner", ""+listEvent.size());
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        // end spinner


        // test button

        button.setVisibility(View.GONE);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getActivity(), ""+ongoingEventsMap.size(), Toast.LENGTH_SHORT).show();

                }
            });

        // end test button




        PushNotifications.start(getActivity().getApplicationContext(), "08cb48fd-2ed5-4cc8-9504-46d63183d9b0");
        PushNotifications.subscribe("cmd_center");

        // dapat dito ko ma eedit yung notification




        Log.d("MySubscriptions: ", PushNotifications.getSubscriptions().toString());

        //Toast.makeText(getContext(), "SourceTree Testing Toast", Toast.LENGTH_SHORT).show();
        return rootView;
    }

    private void clearRecyclerViewData(){
        final int size = mRecyclerView.getChildCount();

        mRecyclerView.removeAllViews();
        mRecyclerView.getAdapter().notifyDataSetChanged();

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }

    @Override
    public void onStart() {
        super.onStart();

        PushNotifications.setOnMessageReceivedListener(new PushNotificationReceivedListener() {
            @Override
            public void onMessageReceived(RemoteMessage remoteMessage) {
//                Log.d(TAG,"Notification received: "+ remoteMessage.getData());
//                Log.d(TAG, "Notification body: "+remoteMessage.getNotification().getBody());
//                Log.d(TAG, "Notification remoteMessage.toString: " + remoteMessage.toString());
//                Log.d(TAG, "Notication toString: "+ remoteMessage.getNotification().toString());

                // Toast.makeText(getApplicationContext(), "Triggered OnMessageReceived", Toast.LENGTH_SHORT).show();
                //

                // dito ko ilalagay yung dapat di mag send sa sarili
                Log.d("MessageType:  ", remoteMessage.getData().get("type"));
                if(!mAuth.getUid().equals(remoteMessage.getData().get("sender"))){
                    SendReceivedNotifications(remoteMessage);
                }





            }
        });
    }

    private void SendReceivedNotifications(RemoteMessage message) {
        Log.d("SendReceivedNotifs: ", "trigurd");


        // dito ko ilalagay yung sa switch ng message type
        // if new event dapat mag papakita yung kung mag rerespond ba siya
        Intent intentEvent = new Intent(getContext(), MyDialog.class);
        intentEvent.putExtra("eventId", message.getData().get("sender"));
        intentEvent.putExtra("message", message.getData().get("body"));

        // firestore chat dapat to
        //Intent intentMessage = new Intent(getContext(), ChatlistActivity.class);
        Intent intent = new Intent(getContext(), MainMenu.class);


        intentEvent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        // originally get context unang parameter neto
        PendingIntent pendingIntent = PendingIntent.getActivity(chatlistContext, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);;


         if(message.getData().get("type").equals("event")) {
             pendingIntent = PendingIntent.getActivity(chatlistContext, 0 /* Request code */, intentEvent,
                     PendingIntent.FLAG_ONE_SHOT);
//             Toast.makeText(getActivity(), message.getData().get("message"), Toast.LENGTH_SHORT).show();
             Log.d("Notification: ", message.getData().get("type"));
             Log.d("Notification: ", message.getData().get("title"));
             Log.d("Notification: ", message.getData().get("body"));
         }else if(message.getData().get("type").equals("message")) {


             if( ongoingEventsMap.containsKey(message.getData().get("channel"))){
                 Log.d("PindotNotif", "meron");
             }else
                 Log.d("PindotNotif", "wala");


             intent = StartChatIntent(ongoingEventsMap.get(message.getData().get("channel")), chatlistContext);
             pendingIntent = PendingIntent.getActivity(chatlistContext, 0 /* Request code */, intent,
                     PendingIntent.FLAG_ONE_SHOT);
         }else if(message.getData().get("type").equals("respond")) {
             //Toast.makeText(getActivity(), message.getData().get("body"), Toast.LENGTH_SHORT).show();
             Log.d("Notification: ", message.getData().get("type"));
             Log.d("Notification: ", message.getData().get("title"));
             Log.d("Notification: ", message.getData().get("body"));
         }

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(chatlistContext, channelId)
                        .setSmallIcon(R.mipmap.logo1)
                        .setContentTitle(message.getData().get("title"))
                        .setContentText(message.getData().get("body"))
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent)
                        .setPriority(NotificationManager.IMPORTANCE_HIGH );

        NotificationManager notificationManager =
                (NotificationManager) chatlistContext.getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_HIGH );
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }


    private void attachRecyclerViewAdapter() {
        listEvent = new ArrayList<>();
        final RecyclerView.Adapter adapter = newAdapter();

        // Scroll to bottom on new messages
        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                // mRecyclerView.smoothScrollToPosition(adapter.getItemCount());
                mRecyclerView.smoothScrollToPosition(1);
            }
        });

        mRecyclerView.setAdapter(adapter);
    }

    protected RecyclerView.Adapter newAdapter() {

        FirestoreRecyclerOptions<Event> options =
                new FirestoreRecyclerOptions.Builder<Event>()
                        .setQuery(sChatQuery, Event.class)
                        .setLifecycleOwner(getActivity())
                        .build();

        return new FirestoreRecyclerAdapter<Event, EventHolder>(options) {

            public void testing(){}


            @Override
            public EventHolder onCreateViewHolder(ViewGroup parent, int viewType) {


                return new EventHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_item_event, parent, false));
            }

            @Override
            protected void onBindViewHolder(@NonNull EventHolder holder, int position, @NonNull Event model) {

                final Event tempModel = model;
                Log.d(TAG, "Event: "+ model.getEventName() +" added!");
                PushNotifications.subscribe(tempModel.getEventId());
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //Toast.makeText(getApplicationContext(), tempModel.getEventId(), Toast.LENGTH_SHORT).show();
                        StartChat(tempModel);
                    }
                });




                holder.bind(model);

                Log.d("SpinnerTest", spinner.getSelectedItem().toString());
//                if(!listEvent.contains(tempModel))
//                    listEvent.add(tempModel);
//
//                if(!allEventsList.contains(tempModel))
//                    allEventsList.add(tempModel);
//
//                if(!ongoingEvents.contains(new Event(tempModel.getEventId())) &&  ){
//                    ongoingEvents.add(tempModel);
//
//                }
                if(spinner.getSelectedItem().toString().equals("Ongoing Events") && !ongoingEventsMap.containsKey(tempModel.getEventId())){
                    ongoingEventsMap.put(tempModel.getEventId(), tempModel);
                    Log.d("onBindView", ongoingEventsMap.size()+"");
                }


            }

            @Override
            public void onDataChanged() {
                // If there are no chat messages, show a view that invites the user to add a message.


            }




        };
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void StartChat(Event tempModel){
        Intent intent = new Intent(getContext(), FirestoreChatActivity.class);
        intent.putExtra("collectionName", tempModel.getEventId());
        intent.putExtra("eventName", tempModel.getEventName());
        intent.putExtra("eventObject", tempModel);
        GeoNew geoNew = new GeoNew();
        geoNew.setSomething("wala lang");
        geoNew.setGeoPoint(tempModel.getMapLocation());
        //Toast.makeText(ChatlistActivity.this, geoNew.getGeoPoint()+"", Toast.LENGTH_SHORT).show();

        // intent.putExtra("EventObject", tempModel);
        intent.putExtra("date", tempModel.getTimeOfCall().getTime());
        intent.putExtra("mapLocation", geoNew);
        //Toast.makeText(ChatlistActivity.this, tempModel.getTimeOfCall()+"", Toast.LENGTH_SHORT).show();
        Log.d(TAG, "event object added");
        startActivity(intent);
    }


    private Intent StartChatIntent(Event tempModel, Context context){

        Intent intent = new Intent(context, FirestoreChatActivity.class);
        intent.putExtra("collectionName", tempModel.getEventId());
        intent.putExtra("eventName", tempModel.getEventName());
        intent.putExtra("eventObject", tempModel);
        GeoNew geoNew = new GeoNew();
        geoNew.setSomething("wala lang");
        geoNew.setGeoPoint(tempModel.getMapLocation());
        //Toast.makeText(ChatlistActivity.this, geoNew.getGeoPoint()+"", Toast.LENGTH_SHORT).show();

        // intent.putExtra("EventObject", tempModel);
        intent.putExtra("date", tempModel.getTimeOfCall().getTime());
        intent.putExtra("mapLocation", geoNew);

        return intent;

    }

}
