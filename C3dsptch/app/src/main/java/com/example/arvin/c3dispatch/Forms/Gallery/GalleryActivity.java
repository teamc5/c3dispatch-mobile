package com.example.arvin.c3dispatch.Forms.Gallery;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.arvin.c3dispatch.Forms.RequestCode;
import com.example.arvin.c3dispatch.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

//Remember to implement  GalleryAdapter.GalleryAdapterCallBacks to activity  for communication of Activity and Gallery Adapter
public class GalleryActivity extends AppCompatActivity implements GalleryAdapter.GalleryAdapterCallBacks {
    //Deceleration of list of  GalleryItems
    //Read storage permission request code
    private static final int RC_READ_STORAGE = 5;
    GalleryAdapter mGalleryAdapter;
    RecyclerView recyclerViewGallery;
    private Query sPhotosQuery;
    private List<String> imgUrls;
    List<Boolean> selectedItems;
    private FloatingActionButton btnSendReport;


    @Override
    protected void onCreate(Bundle savedInstanceState) {




        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);


        btnSendReport = (FloatingActionButton) findViewById(R.id.button_send_report);
        recyclerViewGallery = (RecyclerView) findViewById(R.id.recyclerViewGallery);
        recyclerViewGallery.setLayoutManager(new GridLayoutManager(this, 2));
        imgUrls = new ArrayList<>();
        selectedItems = new ArrayList<>();

        imgUrls = GalleryListenerStarter.getImgUrls();
        selectedItems = GalleryListenerStarter.getSelectedItems();

   //     Toast.makeText(this, imgUrls.toString()+"", Toast.LENGTH_SHORT).show();

//        imgUrls.add("https://firebasestorage.googleapis.com/v0/b/c3chat-782d3.appspot.com/o/Photos%2FC320180423_020110.jpg?alt=media&token=f8d955fc-f45e-4fef-b4ba-ff28387fbb10");
//        imgUrls.add("https://firebasestorage.googleapis.com/v0/b/c3chat-782d3.appspot.com/o/Photos%2FC320180418_081746.jpg?alt=media&token=e067b65e-1aea-4b2c-b4b3-59b0abadbf9e");





        //setup RecyclerView
        //GetPhotos();


        //Create RecyclerView Adapter

        mGalleryAdapter = new GalleryAdapter(this, imgUrls);
        //set adapter to RecyclerView
        recyclerViewGallery.setAdapter(mGalleryAdapter);
        mGalleryAdapter.notifyDataSetChanged();

        recyclerViewGallery.addOnItemTouchListener(new GalleryItemTouchListener(getApplicationContext(), recyclerViewGallery, new GalleryItemTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                Drawable highlight = getResources().getDrawable(R.drawable.highlight);
                Drawable highlightOff = getResources().getDrawable(R.drawable.highlight_off);
                if (!selectedItems.get(position)) {
                    view.setBackground(highlight);
                    selectedItems.set(position, true);
                } else {
                    view.setBackground(highlightOff);
                    selectedItems.set(position, false);
                }
            }


            @Override
            public void onLongClick(View view, int position) {

            }
        }));


        btnSendReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //


                List<String> photoUrls = new ArrayList<>();

                for(int x=0;x<imgUrls.size();x++){
                    if(selectedItems.get(x)){
                        photoUrls.add(imgUrls.get(x));
                    }
                }

                Intent intent = new Intent();

                intent.putStringArrayListExtra("photos", (ArrayList<String>) photoUrls);
                setResult(RESULT_OK, intent);
                finish();

            }
        });



    }




    //    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        if (requestCode == RC_READ_STORAGE) {
//            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                //Get images
//                galleryItems = GalleryUtils.getImages(this);
//                // add images to gallery recyclerview using adapter
//                mGalleryAdapter.addGalleryItems(galleryItems);
//            } else {
//                Toast.makeText(this, "Storage Permission denied", Toast.LENGTH_SHORT).show();
//            }
//        }
//    }

    public void GetPhotos(){
        imgUrls = new ArrayList<>();
        sPhotosQuery = FirebaseFirestore.getInstance().collection("Events").document("20180405_0948_medical")
                .collection("Photos");
        sPhotosQuery.get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot documentSnapshots) {
                for(DocumentSnapshot doc : documentSnapshots){
                    imgUrls.add(doc.getData().toString());

                    Log.d("GalleryLoadURL", "dumaan sa listener");
                }
            }
        });
    }

    @Override
    public void onItemSelected(int position) {

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mGalleryAdapter.notifyDataSetChanged();

        return super.onTouchEvent(event);
    }
}
