package com.example.arvin.c3dispatch.Forms;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;


import com.example.arvin.c3dispatch.R;


public class FormsMainActivity extends AppCompatActivity {
    String forms[] = new String[]{"Incident Report", "Patient's Assessment Form"};

    private String eventId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forms_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle((Html.fromHtml("<font color=\"#FFFFFF\">" + getString(R.string.app_name) + "</font>")));


        final ListView listView = (ListView) findViewById(R.id.list_forms);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, forms);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                //Toast.makeText(getApplicationContext(), forms[i], Toast.LENGTH_LONG).show();
                switch (i) {
                    case 0:
                        Intent intent = new Intent(getApplicationContext(), IncidentReport.class);
                        intent.putExtra("eventId", eventId);
                        startActivityForResult(intent, 0);
                        break;

                    case 1:
                        Intent intent2 = new Intent(getApplicationContext(), NewPatientAssessment.class);
                        intent2.putExtra("eventId", eventId);
                        startActivityForResult(intent2, 0);
                        break;

                }
            }
        });


        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                eventId = null;
            } else {
                eventId = extras.getString("eventId");

            }

        }


    }
}

