package com.example.arvin.c3dispatch.Chatting;

import android.graphics.PorterDuff;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.RotateDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.arvin.c3dispatch.R;

import com.example.arvin.c3dispatch.AbstractChat;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;

//import com.c3.aaron.c3chat.R;

/**
 * Created by Aaron on 21/02/2018.
 */

public class ChatHolder extends RecyclerView.ViewHolder {
    private final TextView mNameField;
    private final TextView mTextField;
    private final FrameLayout mLeftArrow;
    private final FrameLayout mRightArrow;
    private final RelativeLayout mMessageContainer;
    private final LinearLayout mMessage;
    private final int mGreen300;
    private final int mGray300;
    private ImageView mImgView;
    private FirebaseAuth mAuth;


    public ChatHolder(View itemView) {
        super(itemView);
        mAuth = FirebaseAuth.getInstance();
        mNameField = itemView.findViewById(R.id.name_text);
        mTextField = itemView.findViewById(R.id.message_text);
        mLeftArrow = itemView.findViewById(R.id.left_arrow);
        mRightArrow = itemView.findViewById(R.id.right_arrow);
        mMessageContainer = itemView.findViewById(R.id.message_container);
        mMessage = itemView.findViewById(R.id.message);
        mGreen300 = ContextCompat.getColor(itemView.getContext(), R.color.material_green_300);
        mGray300 = ContextCompat.getColor(itemView.getContext(), R.color.material_gray_300);
        mImgView = itemView.findViewById(R.id.imageView);

    }

    public void bind(AbstractChat chat) {
        if(chat.getType().equals("image")) {

            mImgView.setVisibility(View.VISIBLE);
            mTextField.setVisibility(View.GONE);
            setName(chat.getSender());
            setIsSender(mAuth.getUid().equals(chat.getUid()));
            Picasso.get().load(chat.getUrl()).resize(600,900).placeholder(R.drawable.circle).into(mImgView);
        }
        else
        if(chat.getType().equals("text")) {
            mTextField.setVisibility(View.VISIBLE);
            setName(chat.getSender());
            setText(chat.getMessage());
            //setText(chat.getType());
            setIsSender(mAuth.getUid().equals(chat.getUid()));
            mImgView.setVisibility(View.GONE);


        }
        else
        if(chat.getType().equals("voice") || chat.getType().equals("audio")) {
            mTextField.setVisibility(View.GONE);
            setName(chat.getSender());
            //setText(chat.getMessage());
            //setText(chat.getType());
            setIsSender(mAuth.getUid().equals(chat.getUid()));
            mImgView.setVisibility(View.VISIBLE);
            //Picasso.with(mImgView.getContext()).load(R.drawable)
            mImgView.setImageResource(R.drawable.playicon);


        }

        //Picasso.with(mImgView.getContext()).load("https://firebasestorage.googleapis.com/v0/b/c3chat-782d3.appspot.com/o/Photos%2FC320180301_102023.jpg?alt=media&token=2f583716-d4b0-4ce3-ad6c-25fc33c98a04").into(mImgView);

/*        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        setIsSender(currentUser != null && chat.getUid().equals(currentUser.getUid()));*/
    }
//    public void bind(AbstractChat chat, String Url){
//        setName(chat.getName());
//        setIsSender(GLOBALS.MY_UID.equals(chat.getUid()));
//        //Picasso.with(mImgView.getContext()).load(Url).into(mImgView);
//    }

    private void setName(String name) {
        mNameField.setText(name);
    }

    private void setText(String text) {
        mTextField.setText(text);
    }

    private void setIsSender(boolean isSender) {
        final int color;
        if (isSender) {
            color = mGreen300;
            mLeftArrow.setVisibility(View.GONE);
            mRightArrow.setVisibility(View.VISIBLE);
            mMessageContainer.setGravity(Gravity.END);
        } else {
            color = mGray300;
            mLeftArrow.setVisibility(View.VISIBLE);
            mRightArrow.setVisibility(View.GONE);
            mMessageContainer.setGravity(Gravity.START);
        }

        ((GradientDrawable) mMessage.getBackground()).setColor(color);
        ((RotateDrawable) mLeftArrow.getBackground()).getDrawable()
                .setColorFilter(color, PorterDuff.Mode.SRC);
        ((RotateDrawable) mRightArrow.getBackground()).getDrawable()
                .setColorFilter(color, PorterDuff.Mode.SRC);
    }
}