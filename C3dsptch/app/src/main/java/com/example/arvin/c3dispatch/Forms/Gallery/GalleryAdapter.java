package com.example.arvin.c3dispatch.Forms.Gallery;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.arvin.c3dispatch.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by amardeep on 11/3/2017.
 */

public class GalleryAdapter extends RecyclerView.Adapter <GalleryAdapter.GalleryItemHolder>{
    //Declare GalleryItems List
    private OnItemClicked onClick;
    List<String> imgUrls;
    Context context;
    //Declare GalleryAdapterCallBacks
    GalleryAdapterCallBacks mAdapterCallBacks;

    public GalleryAdapter(Context context, List<String> imgUrls) {
        this.context = context;
        this.imgUrls = imgUrls;
        //get GalleryAdapterCallBacks from contex
        this.mAdapterCallBacks = (GalleryAdapterCallBacks) context;
        //Initialize GalleryItem List

        Picasso.get().setLoggingEnabled(true);

    }





    @Override
    public GalleryItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(R.layout.custom_row_gallery_item, parent, false);
        return new GalleryItemHolder(row);
    }

    @Override
    public void onBindViewHolder(final GalleryItemHolder holder, final int position) {

        Picasso.get()
                //.load(R.drawable.face_1)
                .load(imgUrls.get(position))
                .centerCrop()
                //.resize(600,900)
                .resize(ScreenUtils.getScreenWidth(holder.imageViewThumbnail.getContext()) / 2, ScreenUtils.getScreenHeight(holder.imageViewThumbnail.getContext()) / 3)//Resize image to width half of screen and height 1/3 of screen height
                .into(holder.imageViewThumbnail);

        Log.d("OnBindGallery", "image loaded");
        holder.imageViewThumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //call onItemSelected method and pass the position and let activity decide what to do when item selected
                mAdapterCallBacks.onItemSelected(position);
            }
        });

    }




    @Override
    public int getItemCount() {
        return imgUrls.size();
    }
    public String getUrlString(int position){
        return imgUrls.get(position);
    }

    public class GalleryItemHolder extends RecyclerView.ViewHolder {
        ImageView imageViewThumbnail;

        public GalleryItemHolder(View itemView) {
            super(itemView);
            imageViewThumbnail = itemView.findViewById(R.id.imageViewThumbnail);
//            imageViewThumbnail.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Drawable highlight = context.getResources().getDrawable(R.drawable.highlight);
//                    imageViewThumbnail.setBackground(highlight);
//                }
//            });


        }


    }

    //Interface for communication of Adapter and MainActivity
    public interface GalleryAdapterCallBacks {
        //call this method to notify about item is clicked
        void onItemSelected(int position);
    }

    public void setOnClick(OnItemClicked onClick) {
        this.onClick=onClick;
    }

    public interface OnItemClicked {
        void onItemClick(int position);
    }




}
