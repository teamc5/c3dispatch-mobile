package com.example.arvin.c3dispatch.Chatting.InstantMessage;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.arvin.c3dispatch.R;

/**
 * Created by Aaron on 12/04/2018.
 */

public class InstantMessageHolder extends RecyclerView.ViewHolder {
    private TextView instantMessageView;

    public InstantMessageHolder(View itemView) {
        super(itemView);
        instantMessageView = itemView.findViewById(R.id.im_text_item);
    }

    public void bind(String textToReplaceWith){
        instantMessageView.setText(textToReplaceWith);
    }
}
