package com.example.arvin.c3dispatch.Forms.NewForms;

/**
 * Created by Aaron on 01/04/2018.
 */

public class Item {

    public enum ItemType {
        ONE_ITEM, TWO_ITEM, THREE_ITEM;
    }

    private String name;
    private String input;
    private ItemType type;

    public Item(String name, String input, ItemType type) {
        this.name = name;
        this.input = input;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public ItemType getType() {
        return type;
    }

    public void setType(ItemType type) {
        this.type = type;
    }
}
