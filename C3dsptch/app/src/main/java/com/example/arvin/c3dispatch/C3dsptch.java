package com.example.arvin.c3dispatch;

import android.app.Application;


import com.google.firebase.FirebaseApp;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by Arvin on 12 Feb 2018.
 */

public class C3dsptch extends Application{


    @Override
    public void onCreate() {
        super.onCreate();

        FirebaseApp.initializeApp(this);
      //  FirebaseDatabase.getInstance().setPersistenceEnabled(true);

    }
}
