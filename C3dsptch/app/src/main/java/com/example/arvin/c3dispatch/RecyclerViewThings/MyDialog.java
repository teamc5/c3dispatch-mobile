package com.example.arvin.c3dispatch.RecyclerViewThings;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.arvin.c3dispatch.MyPushNotifications;
import com.example.arvin.c3dispatch.R;
import com.pusher.pushnotifications.PushNotifications;

import java.util.Date;

public class MyDialog extends AppCompatActivity {

    private static final String TAG = "MyDialog" ;

    private Button mOpenDialog;



    //vars
    public String mInput;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_dialog);

        String docId = "";
        String message="";

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                docId = null;
            } else {
                docId = extras.getString("eventId");
                message = extras.getString("message");
                Log.d("MyDialog got: ", docId);

            }
        }
//        Bundle extras = getIntent().getExtras();
//        if(extras.getString("Type").equals("image")){
//            ImageDialog imgDialog = new ImageDialog();
//            Bundle bundle = new Bundle();
//            bundle.putString("url", extras.getString("url"));
//            // imgDialog.get
//            //Toast.makeText(getApplicationContext(), bundle.getString("url"), Toast.LENGTH_LONG).show();
//            imgDialog.setArguments(bundle);
//            imgDialog.show(getFragmentManager(), "imageDialog");
//            //CallDialog dialog = new CallDialog();
//
//        }
      //  else {
            MyCustomDialog dialog = new MyCustomDialog();
            Bundle bundle = new Bundle();
            bundle.putString("docId", docId);
            bundle.putString("message", message);

            dialog.setArguments(bundle);
            dialog.show(getFragmentManager(), "MyCustomDialog");
       // }



       // mOpenDialog = findViewById(R.id.open_dialog);
       // mInputDisplay = findViewById(R.id.input_display);



    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        finish();
        return super.onTouchEvent(event);

    }
}
