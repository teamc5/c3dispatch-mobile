package com.example.arvin.c3dispatch.Chatting.InstantMessage;

/**
 * Created by Aaron on 12/04/2018.
 */

public class InstantMessage {
    private String text;

    public InstantMessage(String text) {
        this.text = text;
    }

    public InstantMessage() {
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
